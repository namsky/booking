@extends('web.layouts.master')
@section('page_title', $name_form->name . ' to ' . $name_to->name)
@section('thumb', asset($from_location->image))
@section('description', 'Book and pay for your Vietnam bus ticket here in under a minute - pay instantly online, same day booking, e-ticket by email')
@section('key_words', 'bus vietnam, bus travel, vietnam bus travel')
@section('content')
    <section id="page-category">
        <div class="container">
            <h1 class="title">Buses From Dalat</h1>
            <form action="" method="GET">
                <div class="row">
                    <div class="col-12 col-lg-3">
                        <div class="input-group">
                            <select class="js-example-basic-single js-states form-control select" name="from_location" data-placeholder="Travelling form...">
                                    <option value="{{$from_location->id}}">{{$from_location->name}}</option>
                            </select>
                            <label class="user-label">Travelling form...</label>
                        </div>
                    </div>
                    <div class="col-12 col-lg-3">
                        <div class="input-group">
                            <select class="js-example-basic-single js-states form-control select" name="to_location" data-placeholder="Travelling to...">
                                    <option value="{{$to_location->id}}">{{$to_location->name}}</option>
                            </select>
                            <label class="user-label">Travelling to...</label>
                        </div>
                    </div>
                    <div class="col-12 col-lg-3">
                        <div class="input-group">
                            <input required=""  type="number" name="quantity_per" autocomplete="off" class="input" id="total-passengers" value="{{request()->quantity_per}}">
                            <label class="user-label">Total Passengers...</label>
                        </div>
                    </div>
                    <div class="col-12 col-lg-3">
                        <div class="input-group">
                            <input required="" type="date" name="travel_date" autocomplete="off" class="input min-date"  value="{{request()->travel_date}}">
                            <label class="user-label">Travel Date...</label>
                        </div>
                    </div>
{{--                    <div class="my-3 row mx-1">--}}
{{--                        <div class="col-12 col-lg-2">--}}
{{--                            <button type="submit" class="btn-violet my-2">View Price</button>--}}
{{--                        </div>--}}
{{--                        <div class="col-12 col-lg-10">--}}
{{--                            <p>We will book the best bus service available on your route, when you want it, take online payment by any card or PayPal, and send your ticket via email - and even same-day bookings are available.</p>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                </div>
            </form>

            <div class="py-2"><em>{{$count_tour}} results for {{$name_form->name}} to {{$name_to->name}}</em></div>

            @if ($seats_taxis->count())
                <div class="row">
                    <div class="col-12">
                        <h2>Line bus</h2>
                        <div class="product">
                            @foreach($seats_taxis as $seats_taxi)
                                <div class="row align-items-center">
                                    <div class="col-md-6">
                                        <div class="box-text">
                                            <p class="mb-1">{{$seats_taxi->name}}</p>
                                            <div class="time-start"><strong>{{$seats_taxi->time_from}}</strong> {{$seats_taxi->location($seats_taxi->from_id)}}</div>
                                            <div class="travel-time my-2 font-italic d-none d-lg-block"><i class="fa-solid fa-arrow-down" ></i> {{$seats_taxi->time}} hours</div>
                                            <div class="time-end"><strong>{{$seats_taxi->time_to}}</strong> {{$seats_taxi->location($seats_taxi->to_id)}}</div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 d-flex">
                                        <div class="box-images p-1"><img src="{{$seats_taxi->thumb}}" alt="xe" class="image-show" data-toggle="modal" data-target=".image-modal-show"></div>
                                        <div class="box-images p-1"><img src="{{$seats_taxi->image_1}}" alt="xe" class="image-show" data-toggle="modal" data-target=".image-modal-show"></div>
                                    </div>
                                    <div class="col-md-2 text-right">
                                        <div class="price font-weight-bold">
                                            @if(session('currency') == 'price')
                                                {{number_format($seats_taxi->price)}} USD
                                            @elseif(session('currency') == 'eur')
                                                {{number_format($seats_taxi->eur)}} EUR
                                            @elseif(session('currency') == 'gbp')
                                                {{number_format($seats_taxi->gbp)}} GBP
                                            @elseif(session('currency') == 'aud')
                                                {{number_format($seats_taxi->aud)}} AUD
                                            @elseif(session('currency') == 'sgd')
                                                {{number_format($seats_taxi->sgd)}} SGD
                                            @elseif(session('currency') == 'krw')
                                                {{number_format($seats_taxi->krw)}} KRW
                                            @endif
                                        </div>
                                        <a href="{{route('w.booking',[$seats_taxi,request()->quantity_per,request()->travel_date])}}" class="buy my-3 btn-violet">book</a>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            @endif

            @if ($private_taxis->count())
                <div class="row mt-4">
                    <div class="col-12">
                        <h2> Rental car </h2>
                        <div class="product">
                            @foreach($private_taxis as $private_taxi)
                                <div class="row align-items-center">
                                    <div class="col-md-6">
                                        <div class="box-text">
                                            <p class="mb-1">{{$private_taxi->name}}</p>
                                            <div class="font-italic">{{$private_taxi->time}} hours</div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 d-flex">
                                        <div class="box-images p-1"><img src="{{$private_taxi->thumb}}" alt="xe" class="image-show" data-toggle="modal" data-target=".image-modal-show"></div>
                                        <div class="box-images p-1"><img src="{{$private_taxi->image_1}}" alt="xe" class="image-show" data-toggle="modal" data-target=".image-modal-show"></div>
                                    </div>
                                    <div class="col-md-2 text-right">
                                        <div class="price font-weight-bold">
                                            @if(session('currency') == 'price')
                                                ${{number_format($private_taxi->price)}} USD
                                            @elseif(session('currency') == 'eur')
                                                {{number_format($private_taxi->eur)}} EUR
                                            @elseif(session('currency') == 'gbp')
                                                {{number_format($private_taxi->gbp)}} GBP
                                            @elseif(session('currency') == 'aud')
                                                {{number_format($private_taxi->aud)}} AUD
                                            @elseif(session('currency') == 'sgd')
                                                {{number_format($private_taxi->sgd)}} SGD
                                            @elseif(session('currency') == 'krw')
                                                {{number_format($private_taxi->krw)}} KRW
                                            @endif
                                        </div>
                                        <a href="{{route('w.booking',[$private_taxi,request()->quantity_per,request()->travel_date])}}" class="buy my-3 btn-violet">book</a>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </section>
@endsection
