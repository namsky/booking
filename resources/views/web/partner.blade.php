@extends('web.layouts.master')

@section('content')
    <div class="row grid">
        <div class="slide">
            @include('web.layouts.includes.left_sidebar')
        </div>
        <!--  -->
        @if($partner)
            <div class="bg-white">
                <h2 class="text-center">Thông tin nha khoa</h2>
                <p class="">Tên nha khoa : {{ $partner->name }}</p>
                <p class="">Email : {{ $partner->email }}</p>
                <p class="">Số điện thoại : {{ $partner->phone }}</p>
                <p class="">Địa chỉ : {{ $partner->address }}</p>
                <div class="post">
                    {{-- <article><img src="{{$blog->image}}" alt=""></article> --}}
                    <section>
                        {!!$partner->content!!}
                    </section>
                </div>
            </div>
        @else
            <p>Không có nội dung</p>
        @endif
    </div>
@endsection
