@extends('web.layouts.master')
@section('page_title', $location != null ? $location->name : $name_from . ' to ' . $name_to)
@section('thumb', asset($location != null ? $location->image : $image))
@section('description',$location != null ? $location->description : 'Book and pay for your Vietnam bus ticket here in under a minute - pay instantly online, same day booking, e-ticket by email')
@section('key_words',$location != null ? $location->name : 'bus vietnam, bus travel, vietnam bus travel')
@section('content')
<section id="page-category">
    <div class="container">
        @if(!empty($location))
            <h1 class="title">Buses From {{$location->name}}</h1>
            <div class="row shadown1 px-2 py-4  p-lg-4 mb-lg-3 m-lg-0 m-1 flex-lg-row-reverse">
                <div class="col-lg-4 col-12">
                    <img src="{{$location->image}}" class="img-fluid" alt="ảnh danh mục">
                </div>
                <div class="col-lg-8 col-12">
                    <p class="mt-3 m-lg-0">
                    <span style="color: rgb(0, 0, 0);">{{$location->name}} is a former French hill station in the highlands of
                        Vietnam, with a beautifully cool climate and fantastic scenery. You can catch buses from
                        Dalat to Saigon, also directly to Saigon's Tan Son Nhat International Airport, Nha Trang,
                        Mui Ne and Hoi An. The bus routes, with times and prices are shown in the tables below –
                        just click on the BOOK button to book your bus ticket.
                    </span>
                    </p>
                </div>
            </div>
        @else
            <h1 class="title">{{$name_from}} to {{$name_to}}</h1>
            <div class="mud-paper mud-elevation-1 mud-card p-4 mb-3" style="overflow: hidden;">
                <p>Details of the private taxis available from {{$name_from}} to {{$name_to}} are shown below.</p>
            </div>
        @endif

        {{-- xe tuyen --}}
        @if ($seats_taxis->count())
            <div class="row mt-4">
                <div class="col-12">
                    <h2>Line bus</h2>
                    <div class="product">
                        @foreach($seats_taxis as $seats_taxi)
                            <div class="row align-items-center">
                                <div class="col-md-6">
                                    <div class="box-text">
                                        <p class="mb-2">{{$seats_taxi->name}}</p>
                                        <div class="time-start"><strong>{{$seats_taxi->time_from}}</strong> {{$seats_taxi->location($seats_taxi->from_id)}}</div>
                                        <div class="travel-time my-2 font-italic d-none d-lg-block"><i class="fa-solid fa-arrow-down" ></i> {{$seats_taxi->time}} hours</div>
                                        <div class="time-end"><strong>{{$seats_taxi->time_to}}</strong> {{$seats_taxi->location($seats_taxi->to_id)}}</div>
                                    </div>
                                </div>
                                <div class="col-md-4 d-flex">
                                    <div class="box-images p-1"><img src="{{$seats_taxi->thumb}}" alt="xe" class="image-show" data-toggle="modal" data-target=".image-modal-show"></div>
                                    <div class="box-images p-1"><img src="{{$seats_taxi->image_1}}" alt="xe" class="image-show" data-toggle="modal" data-target=".image-modal-show"></div>
                                </div>
                                <div class="col-md-2 text-right">
                                    <div class="price font-weight-bold">
                                        @if(session('currency') == 'price')
                                            ${{number_format($seats_taxi->price)}} USD
                                        @elseif(session('currency') == 'eur')
                                            {{number_format($seats_taxi->eur)}} EUR
                                        @elseif(session('currency') == 'gbp')
                                            {{number_format($seats_taxi->gbp)}} GBP
                                        @elseif(session('currency') == 'aud')
                                            {{number_format($seats_taxi->aud)}} AUD
                                        @elseif(session('currency') == 'sgd')
                                            {{number_format($seats_taxi->sgd)}} SGD
                                        @elseif(session('currency') == 'krw')
                                            {{number_format($seats_taxi->krw)}} KRW
                                        @endif
                                    </div>
                                    <a href="{{route('w.booking',[$seats_taxi,1,$date])}}" class="buy my-3 btn-violet">book</a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        @endif
            {{-- xe thue --}}
        @if ($private_taxis->count())
            <div class="row mt-4">
                <div class="col-12">
                    <h2> Rental car </h2>
                    <div class="product">
                        @foreach($private_taxis as $private_taxi)
                            <div class="row align-items-center">
                                <div class="col-md-6">
                                    <div class="box-text">
                                        <p class="mb-2">{{$private_taxi->name}}</p>
                                        <div class="font-italic">{{$private_taxi->time}} hours</div>
                                    </div>
                                </div>
                                <div class="col-md-4 d-flex">
                                    <div class="box-images p-1"><img src="{{$private_taxi->thumb}}" alt="xe" class="image-show" data-toggle="modal" data-target=".image-modal-show"></div>
                                    <div class="box-images p-1"><img src="{{$private_taxi->image_1}}" alt="xe" class="image-show" data-toggle="modal" data-target=".image-modal-show"></div>
                                </div>
                                <div class="col-md-2 text-right">
                                    <div class="price font-weight-bold">
                                        @if(session('currency') == 'price')
                                            {{number_format($private_taxi->price)}} USD
                                        @elseif(session('currency') == 'eur')
                                            {{number_format($private_taxi->eur)}} EUR
                                        @elseif(session('currency') == 'gbp')
                                            {{number_format($private_taxi->gbp)}} GBP
                                        @elseif(session('currency') == 'aud')
                                            {{number_format($private_taxi->aud)}} AUD
                                        @elseif(session('currency') == 'sgd')
                                            {{number_format($private_taxi->sgd)}} SGD
                                        @elseif(session('currency') == 'krw')
                                            {{number_format($private_taxi->krw)}} KRW
                                        @endif
                                    </div>
                                    <a href="{{route('w.booking',[$private_taxi,1,$date])}}" class="buy my-3 btn-violet">book</a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        @endif
    </div>

</section>
@endsection
