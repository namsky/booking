@extends('web.layouts.master')
@section('page_title', 'Proof of Onward Travel for your Vietnam Visa?')
@section('thumb', asset($seo->image))
@section('description',$seo->description)
@section('key_words',$seo->key_words)
@section('content')
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-8">
                <div class="shadown1 p-4">
                    <h1>Proof of Onward Travel for your Vietnam Visa?</h1>
                    <p>No problem - you can book a bus ticket online here, from Saigon to Cambodia - either Phnom Penh, Sihanoukville or Siem Reap.</p>
                    <p>
                        Our tickets are accepted as proof of onward travel by airlines and Vietnam immigration, which means if you're arriving in Vietnam without a visa, and you're from one of the countries that do not need a pre-arranged visa for stays of up
                        to 14 days (in some cases more), you will have no problems proving to airport staff and immigration officials that you have onward travel arranged.  This is a requirement for anybody arriving without a visa.
                    </p>
                    <p><strong>Nationals of certain countries may visit Vietnam without a visa (but will require an onward ticket - details below:</strong></p>
                    <p><strong>For stays of less than 15 days:</strong> United Kingdom, Germany, France, Spain, Italy, Sweden, Finland, Denmark, Norway, Japan, South Korea, Belarus, Russia</p>
                    <p><strong>For stays of less than 21 days:</strong> Philippines</p>
                    <p><strong>For stays of less than 30 days:</strong> Cambodia, Singapore, Thailand, Malaysia, Laos, Indonesia</p>
                    <p><span style="font-size: 14px; font-style: italic;">These details were correct when published, but please confirm up to date information with your local Vietnamese embassy/consulate before booking travel.</span></p>
                    <p><a href="">Click here to book your onward ticket - Vietnam (Saigon) to Cambodia (Phnom Penh)</a></p>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
