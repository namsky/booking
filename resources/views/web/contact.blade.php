@extends('web.layouts.master')
@section('page_title', 'Contact')
@section('thumb', asset($seo->image))
@section('description', $seo->description)
@section('key_words', $seo->key_words)
@section('content')
<section>
  <div class="container">
      <div class="row">
          <div class="col-md-12 col-lg-8">
              <div class="shadown1 p-4">
                  <h1>Contact Us</h1>
                  <p>
                      Please use the form below if you need to get in touch with us - we will respond as soon as we can.  Alternatively, you can email us at <a href="mailto:quangthuansapa@gmail.com.com">quangthuansapa@gmail.com</a>,
                      or call us on <a href="tel: 0943696611">(+84)  0943696611</a>.  If you can't find the bus route / ticket that you are looking for on our website, please get in touch anyway, we may be able to find it for you.
                      If you have booked a bus ticket and have not had confirmation from us, please allow 24 hours from the time of booking before contacting us - we're working on it!
                  </p>
                  <!--  -->
                  <form action="" id="contactform">
                      <div class="row pt-5">
                          <div class="col-md-12 col-lg-6">
                              <div class="input-group">
                                  <input required="" type="text" name="name" autocomplete="off" class="input" id="name">
                                  <label class="user-label">Full Name*</label>
                              </div>
                          </div>
                          <div class="col-md-12 col-lg-6">
                              <div class="input-group">
                                  <input required="" type="email" name="email" autocomplete="off" class="input" id="email">
                                  <label class="user-label">Email Address*</label>
                              </div>
                          </div>
                      </div>
                      <div class="row pt-lg-3">
                          <div class="col-md-12 col-lg-6">
                              <div class="input-group">
                                  <input required="" type="text" name="phone" autocomplete="off" class="input" id="phone">
                                  <label class="user-label">Phone Number*</label>
                              </div>
                          </div>
                          <div class="col-md-12 col-lg-6">
                              <div class="input-group">
                                  <textarea required="" name="" id="message" cols="60" rows="6" class="textarea"></textarea>
                                  <label class="user-label">Your Message*</label>
                              </div>
                          </div>
                      </div>
                      <div id="g-recaptcha" data-callback="recaptchaCallback"></div>
                      <button type="submit" class="btn-violet my-2">
                          SEND MESSAGE
                      </button>
                  </form>
              </div>
          </div>
          <div class="col-md-12 col-lg-4">
              <div class="shadown1 p-4 mt-4 mt-lg-0">
                  <img src="{{asset('images/limo-interior.jpg')}}" alt="anh" class="img-fluid rounded mb-4 w-100">
                  <img src="{{asset('images/cabin-sleeper.jpg')}}" alt="anh" class="img-fluid rounded mb-4 w-100">
              </div>
          </div>
      </div>
  </div>
</section>
@endsection
