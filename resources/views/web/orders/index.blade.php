@extends('web.layouts.master')
@section('page_title', 'Đặt hàng')
@section('content')
    <div class="container pt-4 pb-3">
        <h3 class="text-center p-3">Thông Tin Thanh Toán</h3>
        <form action="{{route('w.order.store')}}" method="POST">
            @csrf
            <input type="hidden" name="product_id" value="{{$product->id}}">
            <div class="row">
                <div class="col-12 col-lg-7 ">
                    <div class="shadow mb-3 p-3 bg-white rounded">
                        <div class="form-group">
                            <label for="name">Tên :</label>
                            <input type="text" class="form-control" name="name" id="name" required placeholder="Tên người nhận hàng">
                        </div>
                        <div class="form-group">
                            <label for="address">Địa chỉ :</label>
                            <input type="text" class="form-control" name="address" id="address" required placeholder="Địa chỉ nhận hàng">
                        </div>
                        <div class="form-group">
                            <label for="phone">Số điện thoại :</label>
                            <input type="text" class="form-control" name="phone" id="phone" required placeholder="Số điện thoại nhận hàng">
                        </div>
                        <div class="form-group">
                            <label for="note">Ghi chú đơn hàng :</label>
                            <textarea class="form-control" id="note" name="note" rows="3"></textarea>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-5">
                    <div class="shadow mb-3 p-3 bg-white rounded">
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label font-weight-bold">Tên Sản phẩm :</label>
                            <div class="col-sm-8">
                                <input type="text" readonly class="form-control-plaintext font-weight-bold" value="{{$product->name}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label font-weight-bold">Giá :</label>
                            <div class="col-sm-8">
                                <input type="text" readonly class="form-control-plaintext font-weight-bold" value="{{number_format($product->price)}} VNĐ">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label font-weight-bold">Số lượng :</label>
                            <div class="col-sm-8">
                                <input type="text" readonly class="form-control-plaintext font-weight-bold" name="amount" value="{{$amount}}">
                            </div>
                        </div>
                        <hr>
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label font-weight-bold">Tổng thanh toán :</label>
                            <div class="col-sm-8">
                                <input type="text" readonly class="form-control-plaintext font-weight-bold" value="{{number_format($product->price*$amount)}} VNĐ">
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary mb-2 w-100">Đặt Ngay</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
@push('css')
@endpush
