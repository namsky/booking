@extends('web.layouts.master')
@section('page_title', 'Booking ' . $tour->name)
@section('thumb', asset($tour->thumb))
@section('description',$tour->description)
@section('key_words',$tour->key_words)
@section('content')
<form action="{{route('w.post_booking')}}" method="POST">
    @csrf
    <section id="sec-form">
        <div class="container">
            <h1>Book Vietnam Bus Tickets Online</h1>

            <div class="row">
                <div class="col-12 col-lg-3">
                    <div class="input-group">
                        <select class="js-example-basic-single js-states form-control select" name="from_location" data-placeholder="Travelling form...">
                            <option value="{{$tour->from_id}}">{{$tour->location($tour->from_id)}}</option>
                        </select>
                        <label class="user-label">Travelling form...</label>
                    </div>
                </div>
                <div class="col-12 col-lg-3">
                    <div class="input-group">
                        <select class="js-example-basic-single js-states form-control select" name="to_location" data-placeholder="Travelling to...">
                            <option value="{{$tour->to_id}}">{{$tour->location($tour->to_id)}}</option>
                        </select>
                        <label class="user-label">Travelling to...</label>
                    </div>
                </div>
                <div class="col-12 col-lg-3">
                    <div class="input-group">
                        <input required=""  type="number" name="quantity_per" autocomplete="off" class="input" id="quantity_per" value="{{$quantity_per}}">
                        <label class="user-label">Number of Passengers...</label>
                    </div>
                </div>
                <div class="col-12 col-lg-3">
                    <div class="input-group">
                        <input required="" type="date" name="travel_date" autocomplete="off" class="input min-date" id="travel_date" value="{{old('travel_date',$travel_date)}}" min="">
                        <label class="user-label">Travel Date...</label>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="pay">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-lg-9">
                    <!-- chuyến đi + time -->
                    <div class="product">
                        <div class="row align-items-center">
                            @if($tour->type === 1)
                                <div class="col-6">
                                    <div class="box-text">
                                        <strong>{{$tour->time_from}} &nbsp;</strong>{{$tour->fromLocation->name}}
                                        <br>
                                        <strong>{{$tour->time_to}} &nbsp;</strong>{{$tour->toLocation->name}}
                                    </div>
                                </div>
                            @endif
                            <div class=" {{$tour->type === 2 ? 'col-12' : 'col-6'}}">
                                <div class="box-text">
                                    <p class="m-0">{{$tour->name}}</p>
                                    <p>Journey time: {{$tour->time}} hours</p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="d-flex gap-1">
                                    <div class="box-images p-1"><img src="{{$tour->thumb}}" alt="xe" class="image-show" data-toggle="modal" data-target=".image-modal-show"></div>
                                    <div class="box-images p-1"><img src="{{$tour->image_1}}" alt="xe" class="image-show" data-toggle="modal" data-target=".image-modal-show"></div>
                                </div>
                            </div>
                            @if($tour->type === 2)
                                <div class="col-md-6 text-right">
                                    <div class="input-group">
                                        <input required="" type="time" name="travel_hour" autocomplete="off" class="input" >
                                        <label class="user-label">Choose Your Pick-up Time:*</label>
                                    </div>
                                    <p class="is-small my-2">Click on the clock to choose the time</p>
                                </div>
                            @else
                                <input type="text" name="travel_hour" hidden value="{{$tour->time_from}}">
                            @endif
                            <div class="col-12">
                                @if($tour->type === 2)
                                    <p class="my-3">Private Taxi: {{$tour->description}}</p>
                                @else
                                    <p class="my-3">Air-conditioned 34 berth limousine sleeper buses with personal TV screens, privacy curtain, USB charger port and free wifi.</p>
                                @endif
                            </div>
                        </div>
                    </div>
                    <!--  số người + tiền -->
                    <div class="shadown1 my-3 p-3">
                        <p class="m-0"><span id="qty_per">{{$quantity_per}}</span> Passenger(s)</p>
                        @php
                              if(session('currency') == 'eur') {
                                    $price = $tour->eur;
                                    $current = 'EUR';
                                }elseif(session('currency') == 'gbp') {
                                    $price = $tour->gbp;
                                    $current = 'GBP';
                                }elseif(session('currency') == 'aud') {
                                    $price = $tour->aud;
                                    $current = 'AUD';
                                } elseif(session('currency') == 'sgd') {
                                    $price = $tour->sgd;
                                    $current = 'SGD';
                                }elseif(session('currency') == 'krw') {
                                    $price = $tour->krw;
                                    $current = 'KRW';
                                } else {
                                    $price = $tour->price;
                                    $current = 'USD';
                                }
                        @endphp
                        @if ($tour->type == 1)
                            <p class="m-0 font-weight-bold text-dark">Total Price: $<span id="total_money">{{number_format($quantity_per*$price)}}</span> {{$current}}</p>
                        @else
                            <p class="m-0 font-weight-bold text-dark">Total Price: $<span>{{number_format($price)}}</span> {{$current}}</p>
                        @endif
                    </div>
                    <div class="row mt-5 ">
                        <div class="col-md-12 col-lg-4">
                            <div class="input-group">
                                <input required="" type="text" name="name" autocomplete="off" class="input" id="name">
                                <label class="user-label">Full Name*</label>
                            </div>
                            <p class="is-small my-2">Your name should match the name shown in your passport, only English characters</p>
                        </div>
                        <div class="col-md-12 col-lg-4">
                            <div class="input-group">
                                <input required="" type="email" name="email" autocomplete="off" class="input" id="email">
                                <label class="user-label">Email Address*</label>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-4">
                            <div class="input-group">
                                <input required="" type="text" name="phone" autocomplete="off" class="input" id="phone">
                                <label class="user-label">Phone Number*</label>
                            </div>
                            <p class="is-small my-2">Enter an international phone number starting with +, like this: (+84)0943696611</p>
                        </div>

                    </div>
                    <div class="row pt-2">
                        <div class="col-md-12 col-lg-6">
                            <div class="input-group">
                                <textarea required="" name="address" id="" cols="60" rows="3" class="textarea"></textarea>
                                <label class="user-label">Pick-Up Address*</label>
                            </div>
                            <p class="is-small my-2">You will be picked up from your hotel. Please wait for the driver to find you at reception</p>
                        </div>
                        <div class="col-md-12 col-lg-6">
                            <div class="input-group">
                                <textarea required="" name="address_to" id="" cols="60" rows="3" class="textarea"></textarea>
                                <label class="user-label">Departure terminal*</label>
                            </div>
                            <p class="is-small my-2">We will drop you at your preferred airport terminal</p>
                        </div>
                    </div>
                    <div class="row pt-2 align-items-center">
                        <input type="hidden" name="tour_id" value="{{$tour->id}}">
                        {{--                        <div class="col-md-12 col-lg-3">--}}
                        {{--                            <div class="input-group">--}}
                        {{--                                <input required="" type="text" name="voucher" autocomplete="off" class="input" id="name">--}}
                        {{--                                <label class="user-label">Voucher Code</label>--}}
                        {{--                            </div>--}}
                        {{--                        </div>--}}
                        <div class="col-md-12 col-lg-3">
                            <button class="btn-violet btn-book">BOOK NOW</button>
                        </div>
                    </div>
                </div>
               <div class="col-lg-3 col-md-12 d-none d-md-block">
                    <div class="shadown1 p-3">
                        <h3>Private Mui Ne Sand Dunes Jeep Tour - 10% Discount</h3>
                          <img src="{{asset('/images/jeeptour2.jpg')}}" alt="anh" class="w-100 img-fluid">
                           <p>Book any bus and get 10% discount on a Mui Ne Sand Dunes Jeep Tour - more information <a href="">here</a></p>
                    </div>
                </div>
            </div>
        </div>
    </section>
</form>
@endsection
@push('scripts')
    <script>

        $(document).ready(function (){
            if(  $("#quantity_per").val() > {{$tour->quantity_per}} ) {
                Swal.fire(
                'Warning',
                'You can only have <span id="qty_limit_per">'+{{$tour->quantity_per}}+'</span> passengers in this vehicle - please change the passenger number or select another vehicle',
                'warning',
                )
                $("#quantity_per").val({{$tour->quantity_per}});
                // $(".btn-book").prop('disabled',true);
                {{--$("#qty_limit_per").html({{$tour->quantity_per}});--}}
                {{--$(".alert-per").show();--}}
            } else {
                // $(".btn-book").prop('disabled',false);
            }

            let toDay = new Date();
            toDay = toDay.toISOString().split("T")[0];
            let travel_date = $("#travel_date").val();
            if(!(toDay >= travel_date)){
                console.log('1');
            }
        });

        $("#quantity_per").change(function (){
           $.ajax({
               type: "GET",
               url: '{{route('w.home.check_limit_per')}}',
               data: {
                   qty: $(this).val(),
                   tour_id: {{$tour->id}},
               }, // serializes the form's elements.
               success: function(res)
               {
                   var data = JSON.parse(res);
                   console.log(data);
                   if(data.error == 'success') {
                       // console.log($("#quantity_per").val());
                       // $(".btn-book").prop('disabled',false);
                       $("#qty_per").html($("#quantity_per").val());
                       $("#total_money").html(addCommas($("#quantity_per").val()*{{$price}}));
                   } else {
                       Swal.fire(
                           'Warning',
                           'You can only have <span id="qty_limit_per">'+data.qty+'</span> passengers in this vehicle - please change the passenger number or select another vehicle',
                           'warning',
                       )
                       $("#quantity_per").val(data.qty);
                       $("#qty_per").html(data.qty);
                       $("#total_money").html(addCommas(data.qty*{{$price}}));
                       // $(".btn-book").prop('disabled',true);
                       // $("#qty_limit_per").html(res.qty);
                       // $(".alert-per").show();
                   }
                   // $(".to_location").html(html);
               }
           });
        });

        // Format Number
        function addCommas(str) {
           var amount = new String(str);
           amount = amount.split("").reverse();

           var output = "";
           for (var i = 0; i <= amount.length - 1; i++) {
               output = amount[i] + output;
               if ((i + 1) % 3 == 0 && (amount.length - 1) !== i)
                   output = ',' + output;
           }
           return output;
        }
    </script>

@endpush
