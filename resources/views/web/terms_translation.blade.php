@extends('web.layouts.master')
@section('page_title', 'Terms and translation')
@section('thumb', asset($seo->image))
@section('description',$seo->description)
@section('key_words',$seo->key_words)
@section('content')
<section id="sec-term">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="shadown1 p-4">
                    <h1>Terms and Conditions of Sale for this Website</h1>
                    <p>Once payment has been received, the bus ticket will be issued immediately, and therefore cannot be refunded. Confirmation will be sent to you within 24 hours.</p>
                    <p>Date/time changes may be possible, subject to availability - if required, please email us as soon as possible with details.</p>
                    <p>Ticket prices on this site are inclusive of tax, service charges and credit card charges - the price shown is the price you pay.</p>
                    <p>If you are travelling with a large amount of luggage or sports equipment, the bus company may charge you an additional fee for this.  We cannot advise on this charge, as it will be charged at time of boarding, directly by the bus operators.</p>
                    <p>Hotel collection is only available on certain routes, and then only for central hotels and those on the bus route.  If your hotel does not qualify, we will advise you where you need to go to wait for your bus.</p>
                    <p>This company acts as a booking agent on behalf of the bus operators and therefore cannot be held responsible or liable for any failings of the bus operators concerned.</p>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
