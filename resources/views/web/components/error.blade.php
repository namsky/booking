@if ($success = session('success'))
    <script>
        Swal.fire(
            'Success',
            '{{$success}}',
            'success',
        )
    </script>
@elseif ($warning = session('warning'))
    <script>
        Swal.fire(
            'Warning',
            '{{$warning}}',
            'warning',
        )
    </script>
@elseif ($error = session('error'))
    <script>
        Swal.fire(
            'Error',
            '{{$error}}',
            'error',
        )
    </script>
@elseif ($info = session('info'))
    <script>
        Swal.fire(
            'Info',
            '{{$info}}',
            'info',
        )
    </script>
@endif
@push('scripts')
    <script>
        setTimeout(() => {
            $('#alerts').addClass("d-none")
        },4000);
    </script>
@endpush
