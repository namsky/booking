@extends('web.layouts.master')
@section('page_title', 'About us')
@section('thumb', asset($seo->image))
@section('description',$seo->description)
@section('key_words',$seo->key_words)
@section('content')
<section id="sec-about">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-8">
                <div class="shadown1 p-4">
                    <h1>About our Buses</h1>
                    <p>
                        We have VIP, limousine, sleeper and standard seat buses available on most routes.  We only use reputable, well established bus companies.
                        All buses are air conditioned and provide free water for all passengers, and many of them have toilets and wifi available. On the sleeper buses, all passengers are provided with a pillow and blanket, free of charge.
                        Where on-bus toilets are not available, then regular comfort breaks will be made.
                    </p>
                    <h2>About our Taxis</h2>
                    <p>
                        We offer a high quality private taxi services between many destinations around Vietnam.  We only used reputable taxi operators throughout Vietnam that we know personally - and we've worked with them for years.
                        Our operators use recent model, clean and comfortable air-conditioned 7, 12 & 16 seater vehicles.
                        We use the fastest, most direct route (expressway where available) and we won't charge you extra for it. We won't charge you waiting time at the airport unless your flight is delayed for more than 5 hours.
                        We won't charge extra for airport pickups. We provide a 24 hour English speaking customer hotline.

                    </p>
                    <h2>About Us</h2>
                    <p>
                        Booking Office is run by Adventure Nam, well established travel specialists based in Vietnam.  The proprieters are Europeans, and have
                        travelled around and lived in Vietnam for more than twelve years. Our offices are in Mui Ne. We've travelled the length and breadth of Vietnam,
                        on buses, trains, planes, cars and motorbikes, so we know quite a bit out the place, and how to get around it best! We're very happy to help you get the most out of this wonderful country.
                    </p>
                </div>
            </div>
            <div class="col-md-12 col-lg-4">
                <div class="shadown1 p-4 mt-4 mt-lg-0">
                    <img src="{{asset('/images/limo-interior.jpg')}}" alt="anh" class="img-fluid rounded mb-4">
                    <img src="{{asset('/images/cabin-sleeper.jpg')}}" alt="anh" class="img-fluid rounded mb-4">
                    <img src="{{asset('/images/sleeper-bus.jpg')}}" alt="anh" class="img-fluid rounded mb-4">
                    <img src="{{asset('/images/sleeper-busext.jpg')}}" alt="anh" class="img-fluid rounded mb-4">
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
