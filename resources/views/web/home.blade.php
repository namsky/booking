@extends('web.layouts.master')
@section('page_title', 'Vietnam Bus Travel')
@section('thumb', asset($seo->image))
@section('description',$seo->description)
@section('key_words',$seo->key_words)
@section('content')
<section id="sec-form">
    <div class="container">
        <h1>Book Vietnam Bus Tickets Online</h1>
        <form action="{{route('w.view_price')}}" method="GET">
            <div class="row">
                <div class="col-12 col-lg-3">
                    <div class="input-group">
                        <select class="js-example-basic-single js-states form-control select from_location" name="from_location" data-placeholder="Travelling form...">
                            <option >Select</option>
                            @foreach($from_locations as $from_location)
                                <option value="{{$from_location->id}}">{{$from_location->name}}</option>
                            @endforeach
                        </select>
                        <label class="user-label">Travelling form...</label>
                    </div>
                </div>
                <div class="col-12 col-lg-3">
                    <div class="input-group">
                        <select class="js-example-basic-single js-states form-control select to_location" name="to_location" data-placeholder="Travelling to...">
                        </select>
                        <label class="user-label">Travelling to...</label>
                    </div>
                </div>
                <div class="col-12 col-lg-3">
                    <div class="input-group">
                        <input required=""  type="number" name="quantity_per" autocomplete="off" class="input" id="total-passengers" value="1" min="1">
                        <label class="user-label">Number of Passengers...</label>
                    </div>
                </div>
                <div class="col-12 col-lg-3">
                    <div class="input-group">
                        <input required="" type="date" name="travel_date" autocomplete="off" class="input min-date"  id="travel-to">
                        <label class="user-label">Travel Date...</label>
                    </div>
                </div>
                <div class="my-3 row mx-1">
                    <div class="col-12 col-lg-2">
                        <button type="submit" class="btn-violet my-2">View Price</button>
                    </div>
                    <div class="col-12 col-lg-10">
                        <p>We will book the best bus service available on your route, when you want it, take online payment by any card or PayPal, and send your ticket via email - and even same-day bookings are available.</p>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>
<section id="category-layout-home">
    <div class="container">
        <div class="row flex-column flex-lg-row-reverse">
            <div class="col-md-12 col-lg-3">
                <div class="shadown1 p-4">
                    <h3>Vietnam is OPEN!</h3>
                    <p>
                        Vietnam is open for international tourism again, most restrictions have been lifted, and most bus services are running normally.
                        For the latest Covid and visa travel advisory information, please visit Vietnam.Travel.
                    </p>
                </div>
                <h3 class="ps-3 pt-3">Your Questions Answered</h3>
                <ul id="accordion" class="accordion shadown1">
                    <li>
                      <div class="link font-weight-bold"><i class="fa-solid fa-circle-question"></i>Who are you?<i class="fa fa-chevron-down"></i></div>
                      <ul class="submenu">
                        <p>
                            Vietnam Bus Tickets is by operated by Adventure Nam, travel specialists based in Mui Ne, Vietnam. Adventure Nam is owned and operated by European residents in Vietnam.
                            Our office is in Mui Ne, we're experts in Vietnamese travel and we're on hand to help you while you're here on holiday.
                        </p>
                      </ul>
                    </li>
                    <li>
                        <div class="link font-weight-bold"><i class="fa-solid fa-circle-question"></i>How quickly can I get a ticket?<i class="fa fa-chevron-down"></i></div>
                        <ul class="submenu">
                            <p>
                                If you book within office hours (7.30am - 5pm) we may be able to get you a bus ticket reservation for the same day - give it a try!
                                You will normally receive your ticket by email within 24 hours of booking - faster for short notice bookings.
                            </p>
                        </ul>
                    </li>
                    <li>
                        <div class="link font-weight-bold"><i class="fa-solid fa-circle-question"></i>Where do I catch the bus?<i class="fa fa-chevron-down"></i></div>
                        <ul class="submenu">
                            <p>
                                That depends on where you're leaving from! Also (more seriously), it depends on bus ticket availability with different companies.
                                It will either be from a bus office, or you'll be collected from outside your hotel - if this is the case, you'll be prompted for the
                                hotel name and address  at time of booking, so make sure that information is correct when you book your ticket.
                            </p>
                            <p>
                                Don't worry, we'll give you full information on how to find your bus departure point when we email your bus ticket voucher, after you've booked.
                            </p>
                        </ul>
                    </li>
                    <li>
                        <div class="link font-weight-bold"><i class="fa-solid fa-circle-question"></i>How can I pay?<i class="fa fa-chevron-down"></i></div>
                        <ul class="submenu">
                            <p>
                                That depends on where you're leaving from! Also (more seriously), it depends on bus ticket availability with different companies.
                                It will either be from a bus office, or you'll be collected from outside your hotel - if this is the case, you'll be prompted for the
                                hotel name and address  at time of booking, so make sure that information is correct when you book your ticket.
                            </p>
                            <p>
                                Just complete the booking form on the booking page and press Book Now, and we'll collect payment details on the next page.
                            </p>
                        </ul>
                    </li>
                    <li>
                        <div class="link font-weight-bold"><i class="fa-solid fa-circle-question"></i>What are the sleeper buses like?<i class="fa fa-chevron-down"></i></div>
                        <ul class="submenu">
                            <p>
                                Generally, brilliant. Pretty comfortable, so much nicer than a standard seated bus, we love them. But if you’re over 6 foot (1.83cm) you won’t be able to fully stretch out.
                                It still beats having your knees sticking in the back of the seat in front of you though.
                            </p>
                        </ul>
                    </li>
                    <li>
                        <div class="link font-weight-bold"><i class="fa-solid fa-circle-question"></i>What are the seated buses like?<i class="fa fa-chevron-down"></i></div>
                        <ul class="submenu">
                            <p>
                                Often very comfortable, with quite a bit of recline, and reasonable legroom.  But please note the “often” – some routes just don’t have great buses on them,
                                and sometimes the buses need to be small, because the roads are small.
                            </p>
                            <p>
                                We are now offering limousine buses on several routes, and these are really fabulous - business-class airline style reclining seats, loads of legroom, personal USB chargers,
                                wifi and in some cases even your own "back of the seat" TV screen with entertainment system and headphones.
                                They are a bit more expensive but definitely worth the spend - these limousine VIP buses are our all-time favourites!
                            </p>
                            <p>
                                We do our best to find the best buses on all routes throughout Vietnam, but no guarantees!
                            </p>
                        </ul>
                    </li>
                    <li>
                        <div class="link font-weight-bold"><i class="fa-solid fa-circle-question"></i>I can't find the bus route I want!<i class="fa fa-chevron-down"></i></div>
                        <ul class="submenu">
                            <p>
                                Then please <a href="contact">contact us</a> with exact details of the bus in Vietnam that you're looking for, and we'll do our best to get you the ticket you want.
                            </p>
                        </ul>
                    </li>
                  </ul>
            </div>
            <div class="col-lg-9 col-md-12">
                <div class="row">
                    @foreach($from_locations as $location)
                    <div class="col-lg-4 col-md-12 text-center">
                        <a href="{{route('w.category',$location)}}">
                            <div class="box-category">
                                <div class="box-images">
                                    <img src="{{$location->image}}" alt="{{$location->name}}" class="w-100 rounded">
                                </div>
                                <div class="box-text">
                                    <h3 class="name">Buses from {{$location->name}}</h3>
                                </div>
                            </div>
                        </a>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@push('scripts')
    <script>
        $('.select-money-mobile').change(function (){
            // console.log(1);
            $('#form-select-money-mobile').submit();
        })
        $('.from_location').change(function (){
            $.ajax({
                type: "GET",
                url: '{{route('w.home.find_to_location')}}',
                data: {
                    from: $(this).val(),
                }, // serializes the form's elements.
                success: function(res)
                {
                    var data = JSON.parse(res);
                    var html ='';
                    if (data.data.length > 0) {
                        jQuery.each( data.data, function( i, val ) {
                            html+= '<option value="'+val.id+'">'+val.name+'</option>';
                        });

                    }
                    $(".to_location").html(html);
                }
            });
        })
    </script>
@endpush
