@extends('web.layouts.master')
@section('page_title', 'Category child')
@section('thumb', asset('web/assets/images/banner1.png'))
@section('content')
    <section>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1 class="mb-2 mt-2">Dalat to Ho Chi Minh City (Saigon)</h1>
                    <hr>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <h2 class="mb-2 mt-2">Dalat to Ho Chi Minh City (Saigon) Schedule</h2>
                    <h4 class="mb-3"> Best Value Options</h4>
                </div>
                <div class="col-12">
                    <div class="product">
                        <div class="row align-items-center">
                            <div class="col-md-6">
                                <div class="box-text">
                                    <div class="time-start"><strong>09:00</strong> Dalat</div>
                                    <div class="travel-time my-2 font-italic"><i class="fa-solid fa-arrow-down" ></i> 8 hours</div>
                                    <div class="time-end"><strong>15:00</strong> Ho Chi Minh City (Saigon)</div>
                                </div>
                            </div>
                            <div class="col-md-4 d-flex">
                                <div class="box-images p-1"><img src="{{asset('/images/29cho.png')}}" alt="xe"></div>
                                <div class="box-images p-1"><img src="{{asset('/images/sleeper-bus.jpg')}}" alt="xe"></div>
                            </div>
                            <div class="col-md-2 text-right">
                                <div class="price font-weight-bold">$149.00 USD</div>
                                <a href="{{route('w.booking')}}" class="buy my-3 btn-violet">book</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="product">
                        <div class="row align-items-center">
                            <div class="col-md-6">
                                <div class="box-text">
                                    <div class="time-start"><strong>09:00</strong> Dalat</div>
                                    <div class="travel-time my-2 font-italic"><i class="fa-solid fa-arrow-down" ></i> 8 hours</div>
                                    <div class="time-end"><strong>15:00</strong> Ho Chi Minh City (Saigon)</div>
                                </div>
                            </div>
                            <div class="col-md-4 d-flex">
                                <div class="box-images p-1"><img src="{{asset('/images/29cho.png')}}" alt="xe"></div>
                                <div class="box-images p-1"><img src="{{asset('/images/sleeper-bus.jpg')}}" alt="xe"></div>
                            </div>
                            <div class="col-md-2 text-right">
                                <div class="price font-weight-bold">
                                    @if(session('currency') == 'price')
                                        ${{number_format($tour->price)}} USD
                                    @elseif(session('currency') == 'eur')
                                        {{number_format($tour->eur)}} EUR
                                    @elseif(session('currency') == 'gbp')
                                        {{number_format($tour->gbp)}} GBP
                                    @elseif(session('currency') == 'aud')
                                        {{number_format($tour->aud)}} AUD
                                    @elseif(session('currency') == 'sgd')
                                        {{number_format($tour->sgd)}} SGD
                                    @elseif(session('currency') == 'krw')
                                        {{number_format($tour->krw)}} KRW
                                    @endif
                                </div>
                                <a href="{{route('w.booking')}}" class="buy my-3 btn-violet">book</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
