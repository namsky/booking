@extends('web.layouts.master')
@section('page_title', 'Mui Ne Jeep Tour')
@section('thumb', asset($seo->image))
@section('description',$seo->description)
@section('key_words',$seo->key_words)
@section('content')
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-8">
                <div class="shadown1 p-4">
                    <h1>Mui Ne Jeep Tour</h1>
                    <p><strong>Do you want to experience the amazing sunrise or sunset on the top of the beautiful white sand dune? Then book a Mui Ne Jeep Sand Dune Tour with us!</strong></p>
                    <p>
                        For the sunrise tour, you will be collected from your hotel and taken to see the stunning white sand dunes near the beautiful Lotus lake. After you climb up the sand dunes to enjoy the sunrise, your driver will take you to Vietnam's most beautiful road, a fantastic photo opportunity. After that, you will be taken to the red sand dunes where you can rent a sand slide and slide around in the dunes. Then, you will visit the Mui Ne harbour where you can take some fantastic pictures of the harbour and the ocean and watch the fisherman hard at work. The final stop is at the fairy stream where you can stroll through streams to cool your feet and enjoy the unique stone and sand structures formed around the nearby canyon.
                    </p>
                    <p>
                        The sunset tour will visit the same spots, but in reverse order, so you finish with the sunset on the white sand dunes.
                    </p>
                    <p><strong>Price:</strong> $29 USD per jeep (maximum 5 people) - payable in full when you book.  <strong>Book a bus at the same time and get 10% off!</strong></p>
                    <div class="shadown1 p-3 p-lg-4 my-3" style="background-color:#fdfda0;">
                        <p><strong>Special Offer: Make any bus or taxi booking to or from Mui Ne, and we'll give you a 10% discount on your jeep booking price!</strong></p>
                    </div>
                    <p><strong>What is included:</strong> Pick up and drop off at your hotel, Jeep with a driver, approximate 4 hours tour in total.
                    </p>
                    <p><strong>What is excluded:</strong> Any entry fees, quads at the white sand dunes, sand slide at the red sand dunes.
                    </p>
                    <p>
                        Ready to book?!
                    </p>
                    <div class="p-4 text-center">
                        <button class="btn-violet" style="font-size: 14px;">Click Here to Book Your Jeep Tour Now</button>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-lg-4">
                <div class="shadown1 p-4 mt-4 mt-lg-0">
                    <img src="{{asset('/images/jeeptour1.jpg')}}" alt="anh" class="img-fluid rounded mb-4">
                    <img src="{{asset('images/jeeptour5.jpg')}}" alt="anh" class="img-fluid rounded mb-4">
                    <img src="{{asset('/images/jeeptour4.jpg')}}" alt="anh" class="img-fluid rounded mb-4">
                    <img src="{{asset('/images/jeeptour3.jpg')}}" alt="anh" class="img-fluid rounded mb-4">
                    <img src="{{asset('/images/jeeptour2.jpg')}}" alt="anh" class="img-fluid rounded mb-4">
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
