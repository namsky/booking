<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> @yield('page_title') </title>
    <meta name="description" content="@yield('description')" />
    <meta name="keywords" content="@yield('key_words')" />
    <meta name="author" content="Vietnam Bus Travel" />

    <meta property="og:title" content="@yield('page_title')">
    <meta property="og:type" content="website">
    <meta property="og:url" content="{{request()->url()}}">
    <meta property="og:site_name" content="Vietnam Bus Travel" />
    <meta property="og:description" content="@yield('description')" />
    <meta property="og:image" content="@yield('thumb')">
    <meta property="og:image:alt" content="Image of Vietnam Bus Travel" />

    <!-- CSS (Font, Vendor, Icon, Plugins & Style CSS files) -->
    <link rel="icon" type="image/x-icon" href="{{asset('web/images/logo.png')}}">
    <link rel="preconnect" href="https://fonts.googleapis.com/">
    <link rel="preconnect" href="https://fonts.gstatic.com/" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,400;0,500;0,600;0,700;1,400&amp;family=Work+Sans:ital,wght@0,400;0,500;0,600;0,700;1,400&amp;display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('web/asset/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('web/asset/fontawesome/css/all.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('web/asset/animate/animate.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('web/asset/select2/select2.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('web/asset/css/style.css') }}">
    <script src="{{ asset('web/js/sweetalert2@11.js')}}"></script>

    @stack('css')
</head>

<body>
    <div class="wrapper">

        {{-- header --}}
        @include('web.layouts.includes.header')
        <!--end header -->

        @yield('content')
    </div>

    {{-- footer --}}
    @include('web.layouts.includes.footer')
    {{-- end footer --}}

    @include('web/components/error')

    <script src="{{ asset('web/asset/js/jquery.slim.min.js') }}"></script>
    <script src="{{ asset('web/asset/js/jquery.min.js') }}"></script>
    <script src="{{ asset('web/asset/js/popper.min.js') }}"></script>
    <script src="{{ asset('web/asset/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('web/asset/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('web/asset/js/main.js') }}"></script>
    <script src="{{ asset('web/asset/js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('web/asset/select2/select2.min.js') }}"></script>
    <script src="{{ asset('web/asset/js/validate.js') }}"></script>
    <script src="{{ asset('web/asset/js/lazysizes.min.js')}}"></script>

    @stack('scripts')
</body>
</html>
