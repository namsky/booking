    <!-- show image -->
    <div class="modal fade image-modal-show" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <img src="" alt="anh chuyen di">
            </div>
        </div>
    </div>

    {{-- alert --}}
{{--    <div class="modal fade alert alert-warning alert-dismissible alert-per" tabindex="-1"  role="alert" aria-labelledby="exampleModalCenterTitle">--}}
{{--        <div class="d-flex justify-content-between align-items-center ">--}}
{{--            <i class="fa-solid fa-triangle-exclamation mr-3"></i>--}}
{{--            <span>You can only have <span id="qty_limit_per">12</span> passengers in this vehicle - please change the passenger number or select another vehicle</span>--}}
{{--            <button type="button" class="close" data-dismiss="alert" aria-label="Close">--}}
{{--            <span aria-hidden="true">&times;</span>--}}
{{--        </button>--}}
{{--        </div>--}}
{{--    </div>--}}

<div class="button-contact">
    <div class="phonring-alo-phone">
        <div class="bg">
            <a href="tel:0943696611" rel="nofollow"></a>
        </div>
    </div>
    {{-- <div class="phonring-alo-zalo">
        <a href="https://zalo.me/0943696611" target="_blank" rel="nofollow"></a>
    </div> --}}
</div>
<footer class="footer pt-3">
    <div class="pt-3">Phone / Zalo : <a class="footer text-nowrap" href="tel:0943696611">(+84) 0943 696 611</a></div>
    <div class="pt-2">©Copyright 2023. Thiết kế website bởi <a target="_blank" href="https://net5s.vn" style="color: var(--color-yellow);">Net5s</a></div>
</footer>
