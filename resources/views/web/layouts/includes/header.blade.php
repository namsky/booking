<header>
    <div class="header-top d-none d-lg-block">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-6"><a href="/"><img src="{{asset('images/main-logo.png ')}}" alt="logo" class="main-logo"></a></div>
                <div class="col-md-4 d-flex align-items-center py-2">
                    <div id="whatsapp-desktop-img"><img src="{{asset('images/whatsapp-dt.png')}}"></div>
                    <div id="whatsapp-desktop" class="text-white text-center">
                        Phone / Zalo
                        <br>
                        (+84) 943 696611
                    </div>
                    <div id="kakaotalk-desktop-img"><img src="{{asset('images/kakaotalk.png ')}}"></div>
                </div>
                <div class="col-md-2">
                    <form action="{{route('w.home.change_price')}}" method="POST" id="form-select-money-header" class="form-select-money">
                        @csrf
                        <div class="form-group mb-0">
                            <select class="form-control select-money" name="select_money" id="select-money">
                                <option {{session('currency') == 'price' ? 'selected' : ''}} value="price">USD $</option>
                                <option {{session('currency') == 'eur' ? 'selected' : ''}} value="eur">EUR €</option>
                                <option {{session('currency') == 'gbp' ? 'selected' : ''}} value="gbp">GBP £</option>
                                <option {{session('currency') == 'aud' ? 'selected' : ''}} value="aud">AUD $</option>
                                <option {{session('currency') == 'sgd' ? 'selected' : ''}} value="sgd">SGD $</option>
                                <option {{session('currency') == 'krw' ? 'selected' : ''}} value="krw">KRW ₩</option>
                            </select>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="header-bottom mb-3">
        <div class="container">
            <nav class="navbar navbar-expand-lg navbar-light p-0 flex">
                <button class="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                    aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon text-white"></span>

                </button>
                <a class="navbar-brand d-lg-none logo" href="/" style="width: 80%">
                    <img src="{{asset('images/main-logo.png ')}}" alt="logo" class="main-logo">
                </a>
                <div class="collapse navbar-collapse d-lg-flex flex-lg-row-reverse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto flex-grow-1 me-auto nav-fill w-100">
                        <li class="nav-item active">
                            <a class="nav-link" href="{{route('w.home')}}">BOOK A BUS</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                DEPARTURES
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                @foreach($from_home_locations as $location)
                                    <a class="dropdown-item nav-link" href="{{route('w.category',$location)}}">{{$location->name}}</a>
                                @endforeach
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                TIMETABLES
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                @foreach($merge_datas as $merge_data)
                                    <a class="dropdown-item nav-link" href="{{route('w.category_child',[$merge_data['from']->id,$merge_data['to']->id])}}">{{$merge_data['from']->name}} TO {{$merge_data['to']->name}}</a>
                                @endforeach
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                ABOUT
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item nav-link" href="{{route('w.about')}}">ABOUT US</a>
                                <a class="dropdown-item nav-link" href="{{route('w.terms_translation')}}">TERMS & CONDITIONS</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                OTHER
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item nav-link" href="{{route('w.other_1')}}">MUI NE SAND DUNES JEEP TOUR</a>
                                <a class="dropdown-item nav-link" href="{{route('w.other_2')}}">VIETNAM VISA ONWARD TICKET</a>
                            </div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('w.contact')}}">CONTACT</a>
                        </li>
                        <li class="nav-item d-none d-lg-block">
                            <a class="nav-link" href="#"  data-toggle="modal" data-target="#cart"><i class="fa-solid fa-cart-shopping"
                                    style="color: #ffffff;"></i></a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>

    {{-- select money mobile --}}
    <div class="d-lg-none">
        <div class="container">
            <div class="row d-block d-lg-none">
                <div class="col-12">
                    <div class="d-flex align-items-center justify-content-end">
                        <div class="cart mx-3">
                            <a class="nav-link" href="#"  data-toggle="modal" data-target="#cart"><i class="fa-solid fa-cart-shopping"
                                style="color: #a203af; font-size: 20px"></i></a>
                        </div>
                        <form action="{{route('w.home.change_price')}}" method="POST" id="form-select-money-mobile" class="form-select-money">
                            @csrf
                            <div class="form-group mb-0">
                                <select class="form-control select-money-mobile" name="select_money" id="select-money">
                                    <option {{session('currency') == 'price' ? 'selected' : ''}} value="price">USD $</option>
                                    <option {{session('currency') == 'eur' ? 'selected' : ''}} value="eur">EUR €</option>
                                    <option {{session('currency') == 'gbp' ? 'selected' : ''}} value="gbp">GBP £</option>
                                    <option {{session('currency') == 'aud' ? 'selected' : ''}} value="aud">AUD $</option>
                                    <option {{session('currency') == 'sgd' ? 'selected' : ''}} value="sgd">SGD $</option>
                                    <option {{session('currency') == 'krw' ? 'selected' : ''}} value="krw">KRW ₩</option>
                                </select>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="cart" tabindex="-1" role="dialog" aria-labelledby="cartLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <div class="d-grid d-md-flex justify-content-md-center">There are no bookings in the basket.</div>
                <div class="mt-4 d-grid d-md-flex justify-content-md-center w-100"><button type="button" class="btn btn-primary" class="close" data-dismiss="modal" aria-label="Close">Continue</button></div>
            </div>
        </div>
        </div>
    </div>
</header>
@push('scripts')
    <script>
        $('.select-money').change(function (){
            // console.log(1);
            $('#form-select-money-header').submit();
        })
    </script>
@endpush
