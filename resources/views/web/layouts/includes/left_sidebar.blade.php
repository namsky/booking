<div class="home-right">
    <div id="splide1" class="splide" aria-label="Splide Basic HTML Example">
        <div class="splide__track">
            <ul class="splide__list">
                @foreach($products_left as $product)
                    <li class="splide__slide">
                        <div class="product-items">
                            <a href="{{route('w.product.product_single', $product->slug)}}">
                                <div class="product-images">
                                    <img src="{{ $product->image }}" alt="{{ $product->name }}">
                                    <!-- <span></span> -->
                                </div>
                            </a>
                            <h2 class="product-title m-0"> <a class="text-uppercase" href="{{route('w.product.product_single', $product->slug)}}">{{ $product->name }}</a></h2>
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
</div>
