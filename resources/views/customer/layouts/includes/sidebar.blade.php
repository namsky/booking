<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-closed" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            <li class="sidebar-toggler-wrapper hide">
                <div class="sidebar-toggler">
                    <span></span>
                </div>
            </li>
            <li class="nav-item {{ request()->is('') ? 'active' : '' }}">
                <a href="{{ route('us.home.index') }}"><i class="fa fa-home"></i> <span class="title">Trang Dashboard</span></a>
            </li>
            <li class="nav-item {{ request()->is('') ? 'active' : '' }}">
                <a href="{{ route('us.client.index') }}"><i class="fa fa-user"></i> <span class="title">Trang Khách hàng</span></a>
            </li>
            <li class="nav-item {{ request()->is('') ? 'active' : '' }}">
                <a href="{{ route('us.client.showresetpass') }}"><i class="fa fa-key"></i> <span class="title">Đổi mật khẩu</span></a>
            </li>

            <!-- END SIDEBAR TOGGLE BUTTON -->
        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
</div>
<!-- END SIDEBAR -->
