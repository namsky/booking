@extends('customer.layouts.master')

@section('page_title', 'Sửa thông tin dịch vụ')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cog font-dark"></i>
                        <span class="caption-subject bold uppercase">@yield('page_title')</span>
                    </div>
                    <div class="actions">
                        <a class="btn btn-circle btn-default" href="{{ route('us.home.index') }}" title="">Quay lại</a>
                        <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:void(0);" title="Toàn màn hình"></a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <form action="{{ route('us.service.update', $service) }}" method="POST" @submit.prevent="onSubmit" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="row">
                            <div class="col-12">
                                <div class="row">
                                    <div class="form-group col-md-6" :class="[errors.has('product_id') ? 'has-error' : '']">
                                        <label for="product_id">Tên dịch vụ<span class="required">(Bắt buộc)</span></label>
                                        <select name="product_id" class="form-control" id="product_id">
                                            @foreach($products as $product)
                                                <option value="{{ $product->id }}" {{$service->product_id == $product->id?'selected':''}} v-validate="'required'" data-vv-as="Tên sản phẩm">{{ $product->name }}</option>
                                            @endforeach
                                        </select>
                                        <span class="help-block" v-if="errors.has('product_id')">@{{ errors.first('product_id') }}</span>
                                    </div>
                                    <div class="form-group col-md-6" :class="[errors.has('origin') ? 'has-error' : '']">
                                        <label for="origin">Xuất xứ<span class="required">*</span></label>
                                        <input type="text" id="origin" class="form-control" name="origin" value="{{ $service->origin }}">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6" :class="[errors.has('date_buy') ? 'has-error' : '']">
                                        <label for="date_buy">thời gian bảo hành<span class="required">*</span><small>(tháng/ngày/năm)</small></label>
                                        <input type="date" id="date_buy" class="form-control" name="date_buy" v-validate="'required'" data-vv-as="&quot;Thời gian bảo hành&quot;" value="{{ $service->date_buy }}">
                                        <span class="help-block" v-if="errors.has('date_buy')">@{{ errors.first('date_buy') }}</span>
                                    </div>
                                    <div class="form-group col-md-6" :class="[errors.has('date_expired') ? 'has-error' : '']">
                                        <label for="date_expired">giá trị bảo hành <span class="required">*</span><small>(tháng/ngày/năm)</small></label>
                                        <input type="date" id="date_expired" class="form-control" name="date_expired" v-validate="'required'" data-vv-as="&quot;Giá trị bảo hành &quot;" value="{{ $service->date_expired }}">
                                        <span class="help-block" v-if="errors.has('date_expired')">@{{ errors.first('date_expired') }}</span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6" :class="[errors.has('dental_name') ? 'has-error' : '']">
                                        <label for="dental_name">Vị trí răng</label>
                                        <input type="text" id="dental_name" class="form-control" name="dental_name" value="{{ $service->dental_name }}">
                                    </div>
                                    <div class="form-group col-md-6" :class="[errors.has('supplier') ? 'has-error' : '']">
                                        <label for="supplier">Nhà cung cấp</label>
                                        <input type="text" id="supplier" class="form-control" name="supplier" value="{{ $service->supplier }}">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-12" :class="[errors.has('description') ? 'has-error' : '']">
                                        <label for="description">Nội dung bảo hành</label>
                                        <textarea name="description" class="form-control" cols="30" rows="10">{{ $service->description }}</textarea>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <label for="image">Hình ảnh </label>
                                        <div>
                                            <div class="fileinput fileinput-{{ $service->image ? 'exists' : 'new' }}" data-provides="fileinput">
                                                <div class="fileinput-new thumbnail" style="width: 200px;">
                                                    <img class="img-responsive" src="{{ asset('images/no_image.png') }}" alt="" />
                                                </div>
                                                <div class="fileinput-preview fileinput-exists thumbnail" style="height: 200px">
                                                    @if($service->image)
                                                        <img class="img-responsive" src="{{ $service->image }}" alt="Preview banner"/>
                                                    @endif
                                                </div>
                                                <div>
                                                    <span class="btn default btn-file">
                                                        <span class="fileinput-new">Chọn ảnh</span>
                                                        <span class="fileinput-exists">Đổi ảnh</span>
                                                        <input type="file" name="image">
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="client_id" id="client_id" value="{{$service->client_id}}">
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn btn-primary">Cập nhật</button>
                            <a href="{{ route('us.home.index') }}" class="btn btn-default">Quay lại</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('css')
<link href="{{ asset('global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css" />
@endpush

@prepend('scripts')
<script src="{{ asset('global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
@endprepend

