@extends('customer.layouts.master')

@section('page_title', 'Trang Dashboard')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light portlet-datatable bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cog"></i>
                        <span class="caption-subject bold uppercase">@yield('page_title')</span>
                    </div>
                    <div class="actions">
                        <a href="{{ route('us.client.create') }}" class="btn btn-circle btn-sm btn-primary"> <i
                                class="fa fa-plus"></i>Thêm khách hàng</a>
                        <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:void(0);"
                            title=""></a>
                    </div>
                </div>

                <div class="portlet-title">
                    <form action="" method="GET">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Số điện thoại</label>
                                    <input type="phone" name="phone" class="form-control" value="{{ old('phone') }}"
                                        id="exampleInputEmail1">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="exampleInputEmail1" style="">&nbsp;</label>
                                    <button class="btn btn-primary form-control" type="submit">Tìm kiếm</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover " id="admin-table">
                        <thead>
                            <tr class="text-capitalize">
                                <th width="3%">ID</th>
                                <th width="10%">Tên khách hàng</th>
                                <th width="7%">Phone</th>
                                <th width="8%">Tên sản phẩm</th>
                                <th width="7%">Ngày mua</th>
                                <th width="10%">Ngày hết hạn bảo hành</th>
                                <th width="15%">Mã Qr</th>
                                <th width="10%">Ghi chú</th>
                                <th width="10%" class="text-center">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($services as $key => $service)
                                <tr>
                                    <td>{{ $key + 1 }}</td>
                                    <td>{{ $service->client->name }}</td>
                                    <td>{{ $service->client->phone }}</td>
                                    <td>{{ $service->product->name }}</td>
                                    <td>{{ date('d/m/Y', strtotime($service->date_buy)) }}</td>
                                    <td>{{ date('d/m/Y', strtotime($service->date_expired)) }}</td>
                                    <td>
                                        <a target="_blank" href="{{ route('w.home.show_qrcode', $service->qrcode->url) }}">{{request()->getHttpHost()}}/bao-hanh/{{ $service->qrcode->url }}</a>
                                    </td>
                                    <td>{{ $service->description }}</td>

                                    <td class="text-center">
                                        <a title="Thêm dịch vụ" class="btn btn-circle btn-sm btn-success"
                                            href="{{ route('us.service.create', $service->client_id) }}"><i
                                                class="fa fa-plus"></i></a>
                                        <a title="Chỉnh sửa" class="btn btn-circle btn-sm btn-warning"
                                            href="{{ route('us.service.edit', $service->id) }}"><i
                                                class="fa fa-pencil"></i></a>

{{--                                        <form title="Xóa" action="{{ route('us.service.destroy', $service->id) }}"--}}
{{--                                            id="delete-course-item-form" style="display: inline-block" method="POST"--}}
{{--                                            onsubmit="return confirm('Bạn có chắc chắn muốn xóa?');">--}}
{{--                                            @csrf--}}
{{--                                            @method('DELETE')--}}
{{--                                            <button class="btn btn-circle btn-sm btn-danger"><i--}}
{{--                                                    class="fa fa-trash"></i></button>--}}
{{--                                        </form>--}}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $services->links() }}
                </div>
            </div>
        </div>
    </div>

@endsection
