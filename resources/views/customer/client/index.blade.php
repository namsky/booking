@extends('customer.layouts.master')

@section('page_title', 'Trang Quản Lý Khách Hàng')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light portlet-datatable bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cog"></i>
                        <span class="caption-subject bold uppercase">@yield('page_title')</span>
                    </div>
                    <div class="actions">
                        <a href="{{ route('us.client.create') }}" class="btn btn-circle btn-sm btn-primary"> <i
                                class="fa fa-plus"></i>Thêm khách hàng</a>
                        <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:void(0);"
                            title=""></a>
                    </div>
                </div>

                <div class="portlet-title">
                    <form action="" method="GET">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Họ Tên</label>
                                    <input type="text" name="name" class="form-control" value="{{ old('name') }}"
                                        id="exampleInputEmail1">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Số điện thoại</label>
                                    <input type="text" name="phone" class="form-control" value="{{ old('phone') }}"
                                        id="exampleInputEmail1">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="exampleInputEmail1" style="">&nbsp;</label>
                                    <button class="btn btn-primary form-control" type="submit">Tìm kiếm</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <hr>
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover" id="admin-table">
                        <thead>
                            <tr>
                                <th width="7%">ID</th>
                                <th width="10%">Tên</th>
                                <th width="10%">Năm sinh</th>
                                <th width="10%">SĐT</th>
                                <th width="10%">Email</th>
                                <th width="7%">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($clients as $key => $client)
                                <tr>
                                    <td>{{ $key + 1 }}</td>
                                    <td>{{ $client->name }}</td>
                                    <td>{{ $client->birthday }}</td>
                                    <td>{{ $client->phone }}</td>
                                    <td>{{ $client->email }}</td>
                                    <td>
                                        <a title="Thêm dịch vụ" class="btn btn-circle btn-sm btn-success"
                                        href="{{ route('us.service.create', $client->id ) }}"><i class="fa fa-plus"></i></a>

                                        <a title="Chỉnh sửa" class="btn btn-circle btn- sm btn-warning"
                                            href="{{ route('us.client.edit', $client) }}"><i class="fa fa-pencil"></i></a>

                                        <form title="Xóa" action="{{ route('us.client.destroy', $client) }}"
                                            id="delete-course-item-form" style="display: inline-block" method="POST"
                                            onsubmit="return confirm('Bạn có chắc chắn muốn xóa?');">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-circle btn-sm btn-danger"><i class="fa fa-trash"></i></button>
                                        </form>

                                        {{-- <a title="Xem chi tiết" class="btn btn-circle btn-sm btn-warning" href="{{ route('us.client.show', $client) }}"><i class="fa fa-user"></i></a> --}}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $clients->links() }}
                </div>
            </div>
        </div>
    </div>

@endsection
