@extends('customer.layouts.master')

@section('page_title', 'Trang Quản Lý Khách Hàng')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light portlet-datatable bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cog"></i>
                        <span class="caption-subject bold uppercase">@yield('page_title')</span>
                    </div>
                    <div class="actions">
                        <a href="{{ route('us.client.index') }}" class="btn btn-circle btn-sm btn-primary"> <i
                                class="fa fa-back"></i>Quay lại</a>
                        <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:void(0);"
                            title=""></a>
                    </div>
                </div>
                <div class="portlet-body text-capitalize">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="card text-dark">
                                <div class="card-body ">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <p class="mb-0 font-weight-bold">tên:</p>
                                        </div>
                                        <div class="col-sm-9">
                                            <p class="mb-0 font-weight-bold">{{ $client->name }}</p>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <p class="mb-0 font-weight-bold">Email:</p>
                                        </div>
                                        <div class="col-sm-9">
                                            <p class="mb-0 font-weight-bold">{{ $client->email }}</p>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <p class="mb-0 font-weight-bold">Phone:</p>
                                        </div>
                                        <div class="col-sm-9">
                                            <p class="mb-0 font-weight-bold">{{ $client->phone }}</p>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <p class="mb-0 font-weight-bold">Địa chỉ:</p>
                                        </div>
                                        <div class="col-sm-9">
                                            <p class="mb-0 font-weight-bold">{{ $client->address }}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <table class="table table-striped table-bordered table-hover" id="admin-table">
                                <thead>
                                    <tr>
                                        <th width="10%">Ngày mua</th>
                                        <th width="10%">Ngày hết hạn</th>
                                        <th width="10%">Tên sản phẩm</th>
                                        <th width="10%">Ảnh sản phẩm</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>{{ date('d/m/Y', strtotime($client->purchase_date)) }}</td>
                                        <td>{{ date('d/m/Y', strtotime($client->expiration_date)) }}</td>
                                        <td>{{ $client->product_name }}</td>
                                        <td><img src="{{ $client->image }}" alt="avatar" class="img-fluid img-thumbnail"
                                                style="width: 150px;"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('css')
@endpush
