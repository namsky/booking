@extends('customer.layouts.master')

@section('page_title', 'Tạo mới khách hàng')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cog font-dark"></i>
                        <span class="caption-subject bold uppercase">@yield('page_title')</span>
                    </div>
                    <div class="actions">
                        <a class="btn btn-circle btn-default" href="{{ route('us.client.index') }}" title="">Quay
                            lại</a>
                        <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:void(0);"
                            title="Toàn màn hình"></a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <form action="{{ route('us.client.store') }}" method="POST" @submit.prevent="onSubmit"
                        enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-md-6">

                                    <div class="form-group" :class="[errors.has('name') ? 'has-error' : '']">
                                        <label for="name">Tên <span class="required">*</span></label>
                                        <input type="text" id="name" class="form-control name_client" name="name"
                                            v-validate="'required'" data-vv-as="&quot;Tên&quot;"
                                            value="{{ old('name') }}">
                                        <span class="help-block" v-if="errors.has('name')">@{{ errors.first('name') }}</span>
                                    </div>
                                    <div class="form-group" :class="[errors.has('birthday') ? 'has-error' : '']">
                                        <label for="birthday">Năm sinh <span class="required">*</span></label>
                                        <input type="number" id="birthday" class="form-control" name="birthday" value="{{ old('birthday') }}" min="0">
                                    </div>
                                    <div class="form-group" :class="[errors.has('email') ? 'has-error' : '']">
                                        <label for="email">Email</label>
                                        <input type="email" id="email" class="form-control" name="email">
                                        <span class="help-block" v-if="errors.has('email')">@{{ errors.first('email') }}</span>
                                    </div>
                                    <div class="form-group" :class="[errors.has('phone') ? 'has-error' : '']">
                                        <label for="phone">Số điện thoại<span class="required">*</span></label>
                                        <input type="text" id="phone" class="form-control" name="phone"
                                            v-validate="'required'" data-vv-as="&quot;Số điện thoại&quot;"
                                            value="{{ old('phone') }}">
                                        <span class="help-block" v-if="errors.has('phone')">@{{ errors.first('phone') }}</span>
                                    </div>
                                    <div class="form-group" :class="[errors.has('address') ? 'has-error' : '']">
                                        <label for="address">Địa chỉ</label>
                                        <input type="text" id="address" class="form-control" name="address" value="{{ old('address') }}">
                                    </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn btn-primary">Tạo mới</button>
                            <a href="{{ route('us.client.index') }}" class="btn btn-default">Quay lại</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('css')
    <link href="{{ asset('global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet"
        type="text/css" />
@endpush

@prepend('scripts')
    <script src="{{ asset('global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
{{--    <script>--}}
{{--        $(".name_client").keyup(function (){--}}
{{--            $(this).val($(this).val().toUpperCase());--}}
{{--        });--}}
{{--    </script>--}}
@endprepend
