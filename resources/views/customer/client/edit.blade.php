@extends('customer.layouts.master')

@section('page_title', 'Tạo mới user')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cog font-dark"></i>
                        <span class="caption-subject bold uppercase">@yield('page_title')</span>
                    </div>
                    <div class="actions">
                        <a class="btn btn-circle btn-default" href="{{ route('us.client.index') }}" title="">Quay
                            lại</a>
                        <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:void(0);"
                            title="Toàn màn hình"></a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <form action="{{ route('us.client.update', $client) }}" method="POST" @submit.prevent="onSubmit"
                        enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="row">
                            <div class="col-md-6">
                                    <div class="form-group" :class="[errors.has('name') ? 'has-error' : '']">
                                        <label for="name">Tên <span class="required">*</span></label>
                                        <input type="text" id="name" class="form-control" name="name"
                                            v-validate="'required'" data-vv-as="&quot;Tên&quot;"
                                            value="{{ $client->name }}">
                                        <span class="help-block" v-if="errors.has('name')">@{{ errors.first('name') }}</span>
                                    </div>
                                    <div class="form-group" :class="[errors.has('birthday') ? 'has-error' : '']">
                                        <label for="birthday">Năm sinh<span class="required">*</span></label>
                                        <input type="text" id="birthday" class="form-control" name="birthday" value="{{ $client->birthday }}">
                                    </div>
                                    <div class="form-group" :class="[errors.has('email') ? 'has-error' : '']">
                                        <label for="email">Email <span class="required">*</span></label>
                                        <input type="email" id="email" class="form-control" name="email"
                                            v-validate="'required'" data-vv-as="&quot;Email&quot;"
                                            value="{{ $client->email }}">
                                        <span class="help-block" v-if="errors.has('email')">@{{ errors.first('email') }}</span>
                                    </div>
                                    <div class="form-group" :class="[errors.has('phone') ? 'has-error' : '']">
                                        <label for="phone">Số điện thoại<span class="required">*</span></label>
                                        <input type="text" id="phone" class="form-control" name="phone"
                                            v-validate="'required'" data-vv-as="&quot;Số điện thoại&quot;"
                                            value="{{ $client->phone }}">
                                        <span class="help-block" v-if="errors.has('phone')">@{{ errors.first('phone') }}</span>
                                    </div>
                                    <div class="form-group" :class="[errors.has('address') ? 'has-error' : '']">
                                        <label for="address">Địa chỉ<span class="required">*</span></label>
                                        <input type="text" id="address" class="form-control" name="address" value="{{ $client->address }}">
                                    </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn btn-primary">Cập nhật</button>
                            <a href="{{ route('us.client.index') }}" class="btn btn-default">Quay lại</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('css')
    <link href="{{ asset('global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet"
        type="text/css" />
@endpush

@prepend('scripts')
    <script src="{{ asset('global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
@endprepend
