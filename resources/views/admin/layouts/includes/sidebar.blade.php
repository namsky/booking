<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-closed" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            <li class="sidebar-toggler-wrapper hide">
                <div class="sidebar-toggler">
                    <span></span>
                </div>
            </li>
            <li class="nav-item {{ request()->is('admin/') ? 'active' : '' }}">
                <a href="{{ route('ad.index') }}"><i class="fa fa-home"></i> <span class="title">Dashboard</span></a>
            </li>

            <li class="nav-item {{ request()->is(['admin/user*', 'admin/partner*']) ? 'active' : '' }}">
                <a href="javascript:void(0);" class="nav-link nav-toggle">
                    <i class="fa fa-user"></i>
                    <span class="title">Quản lý User</span>
                    <span class="arrow {{ request()->is(['admin/user*', 'admin/partner*']) ? 'active' : '' }}"></span>
                </a>
            </li>

            <li class="nav-item {{ request()->is('admin/order*') ? 'active' : '' }}">
                <a href="{{ route('ad.order.index') }}"><i class="fa fa-user"></i> <span class="title">Quản lý đơn hàng</span></a>
            </li>

            <li class="nav-item {{ request()->is(['admin/category*', 'admin/blog*', 'admin/tour*', 'admin/locations*']) ? 'active' : '' }}">
                <a href="javascript:void(0);" class="nav-link nav-toggle">
                    <i class="fa fa-file"></i>
                    <span class="title">Quản lý nội dung</span>
                    <span class="arrow {{ request()->is(['admin/category*', 'admin/blog*']) ? 'active' : '' }}"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item {{ request()->is(['admin/tour*']) ? 'active' : '' }}">
                        <a href="{{ route('ad.tour.index') }}" class="nav-link">
                            <span class="title">Tour</span>
                        </a>
                    </li>
                    <li class="nav-item {{ request()->is(['admin/locations*']) ? 'active' : '' }}">
                        <a href="{{ route('ad.locations.index') }}" class="nav-link">
                            <span class="title">Điểm đi</span>
                        </a>
                    </li>
                    <li class="nav-item {{ request()->is(['admin/locations*']) ? 'active' : '' }}">
                        <a href="{{ route('ad.locations.index') }}" class="nav-link">
                            <span class="title">Điểm đến</span>
                        </a>
                    </li>
                    <li class="nav-item {{ request()->is(['admin/blog*']) ? 'active' : '' }}">
                        <a href="{{ route('ad.blog.index') }}" class="nav-link">
                            <span class="title">Bài viết</span>
                        </a>
                    </li>
                </ul>
            </li>

            <li class="nav-item {{ request()->route()->getName() === 'ad.seo.index'? 'active' : '' }}">
                <a href="{{ route('ad.seo.index') }}"><i class="fa fa-search-plus" aria-hidden="true"></i><span class="title">Quản lý SEO Pages</span></a>
            </li>

            <li class="nav-item {{ request()->is(['admin/admin*']) ? 'active' : '' }}">
                <a href="{{ route('ad.admin.index') }}" class="nav-link nav-toggle">
                    <i class="fa fa-cogs"></i>
                    <span class="title">Quản lý Admin</span>
                </a>
            </li>
            <!-- END SIDEBAR TOGGLE BUTTON -->
        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
</div>
<!-- END SIDEBAR -->
