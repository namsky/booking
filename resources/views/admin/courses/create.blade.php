@extends('admin.layouts.master')

@section('page_title', 'Tạo mới khóa học mới')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase">@yield('page_title')</span>
                    </div>
                    <div class="actions">
                        <a class="btn btn-circle btn-default" href="{{ route('ad.course.index') }}" title="">Quay lại</a>
                        <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:void(0);" title="Toàn màn hình"></a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <form action="{{ route('ad.course.store') }}" method="POST" @submit.prevent="onSubmit" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group" :class="[errors.has('name') ? 'has-error' : '']">
                                    <label for="name">Tên khóa học <span class="required">*</span></label>
                                    <input type="text" id="name" class="form-control" name="name" v-validate="'required'" data-vv-as="&quot;Tên khóa học&quot;" value="{{ old('name') }}">
                                    <span class="help-block" v-if="errors.has('name')">@{{ errors.first('name') }}</span>
                                </div>
                                <div class="form-group" >
                                    <label for="content">Nội dung <span class="required">*</span></label>
                                    <textarea  type="text" id="content" class="form-control tinymce" name="content" >{!! old('content') !!}</textarea>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group" :class="[errors.has('total_time') ? 'has-error' : '']">
                                    <label for="total_time">Thời gian học <span class="required">*</span></label>
                                    <input type="text" id="total_time" class="form-control" name="total_time" v-validate="'required'" data-vv-as="&quot;Thời gian học&quot;" value="{{ old('total_time') }}">
                                    <span class="help-block" v-if="errors.has('total_time')">@{{ errors.first('total_time') }}</span>
                                </div>
                                <div class="form-group" :class="[errors.has('total_lessons') ? 'has-error' : '']">
                                    <label for="total_lessons">Số bài học <span class="required">*</span></label>
                                    <input type="text" id="total_lessons" class="form-control" name="total_lessons" v-validate="'required'" data-vv-as="&quot;Số bài học&quot;" value="{{ old('total_lessons') }}">
                                    <span class="help-block" v-if="errors.has('total_lessons')">@{{ errors.first('total_lessons') }}</span>
                                </div>
                                <div class="form-group" :class="[errors.has('new_price') ? 'has-error' : '']">
                                    <label for="new_price">Giá bán<span class="required">*</span></label>
                                    <input type="number" id="new_price" class="form-control" name="new_price" v-validate="'required'" data-vv-as="&quot;Giá bán&quot;" value="{{ old('new_price') }}">
                                    <span class="help-block" v-if="errors.has('new_price')">@{{ errors.first('new_price') }}</span>
                                </div>
                                <div class="form-group" >
                                    <label for="price">Giá gốc</label>
                                    <input type="number" id="price" class="form-control" name="price"  value="{{ old('price') }}">
                                </div>
                                <div class="form-group" >
                                    <label for="auth">Giảng viên</label>
                                    <input type="text" id="auth" class="form-control" name="auth"  value="{{ old('auth') }}">
                                </div>
                                <div class="form-group" >
                                    <label for="keywords">Keywords</label>
                                    <input type="text" id="keywords" class="form-control" name="keywords"  value="{{ old('keywords') }}">
                                </div>
                                <div class="form-group" :class="[errors.has('link') ? 'has-error' : '']">
                                    <label for="link">Liên kết<span class="required">*</span></label>
                                    <input type="text" id="link" class="form-control" name="link" v-validate="'required'" data-vv-as="&quot;Liên kết&quot;" value="{{ old('link') }}">
                                    <span class="help-block" v-if="errors.has('link')">@{{ errors.first('link') }}</span>
                                </div>
                                <div class="form-group" >
                                    <label for="position">Vị trí</label>
                                    <input type="number" id="position" class="form-control" name="position"  value="{{ old('position') }}">
                                </div>
                                

                                <div class="form-group">
                                    <label for="image">Hình ảnh <span class="required">*</span></label>
                                    <div>
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail" style="width: 200px;">
                                                <img class="img-responsive" src="{{ asset('images/no_image.png') }}" alt="" />
                                            </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail" style="height: 200px"></div>
                                            <div>
                                                <span class="btn default btn-file">
                                                    <span class="fileinput-new">Chọn ảnh</span>
                                                    <span class="fileinput-exists">Đổi ảnh</span>
                                                    <input type="file" accept="image/*" name="image">
                                                </span>
                                                <a href="javascript:void(0);" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">Xóa</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" :class="[errors.has('description') ? 'has-error' : '']">
                                    <label for="description">Mô tả <span class="required">*</span></label>
                                    <textarea  type="text" id="description" class="form-control" name="description" v-validate="'required'" data-vv-as="&quot;Mô tả&quot;">{{ old('description') }}</textarea>
                                    <span class="help-block" v-if="errors.has('description')">@{{ errors.first('description') }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <button class="btn btn-primary">Tạo mới</button>
                            <a href="{{ route('ad.course.index') }}" class="btn btn-default">Quay lại</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('css')
<link href="{{ asset('global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css" />
@endpush

@prepend('scripts')
<script src="{{ asset('global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
@include('admin.lib.tinymce-setup')

@endprepend

