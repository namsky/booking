@extends('admin.layouts.master')

@section('page_title', 'Trang Dashboard')

@section('content')
{{--    <div class="row">--}}
{{--        <div class="col-md-12">--}}
{{--            <div class="portlet light portlet-datatable bordered">--}}
{{--                <div class="portlet-title">--}}
{{--                    <div class="caption">--}}
{{--                        <i class="fa fa-cog"></i>--}}
{{--                        <span class="caption-subject bold uppercase">@yield('page_title')</span>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="portlet-title">--}}
{{--                    <form action="" method="GET">--}}
{{--                        <div class="row">--}}
{{--                            <div class="col-md-4">--}}
{{--                                <div class="form-group">--}}
{{--                                    <label for="exampleInputEmail1">Số điện thoại</label>--}}
{{--                                    <input type="phone" name="phone" class="form-control" value="{{ old('phone') }}"--}}
{{--                                        id="exampleInputEmail1">--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="col-md-4">--}}
{{--                                <div class="form-group">--}}
{{--                                    <label for="exampleInputEmail1">Đại lý</label>--}}
{{--                                    <select class="form-control" name="user_id" id="">--}}
{{--                                        <option value="">Tất cả</option>--}}
{{--                                        @foreach ($users as $user)--}}
{{--                                            <option value="{{ $user->id }}" {{ old('user_id') == $user->id ? 'selected' : '' }}>{{$user->name}}</option>--}}
{{--                                        @endforeach--}}
{{--                                    </select>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="col-md-2">--}}
{{--                                <div class="form-group">--}}
{{--                                    <label for="exampleInputEmail1" style="">&nbsp;</label>--}}
{{--                                    <button class="btn btn-primary form-control" type="submit">Tìm kiếm</button>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="col-md-1">--}}
{{--                                <div class="form-group">--}}
{{--                                    <label for="exampleInputEmail1" style="">&nbsp;</label>--}}
{{--                                    <a class="btn btn-primary form-control" href="{{route('ad.index')}}">Reset</a>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </form>--}}
{{--                </div>--}}

{{--                <div class="portlet-body">--}}
{{--                    <table class="table table-striped table-bordered table-hover text-wrap" id="admin-table">--}}
{{--                        <thead>--}}
{{--                            <tr class="text-capitalize ">--}}
{{--                                <th width="3%">ID</th>--}}
{{--                                <th width="7%">Tên đại lý</th>--}}
{{--                                <th width="10%">Tên khách hàng</th>--}}
{{--                                <th width="7%">Năm sinh</th>--}}
{{--                                <th width="10%">Địa chỉ</th>--}}
{{--                                <th width="7%">Sđt</th>--}}
{{--                                <th width="10%">Tên dịch vụ</th>--}}
{{--                                <th width="7%">Xuất xứ</th>--}}
{{--                                <th width="10%">Thời gian bảo hành <small>(ngày/tháng/năm)</small></th>--}}
{{--                                <th width="10%">Giá trị bảo hành  <small>(ngày/tháng/năm)</small></th>--}}
{{--                                <th width="7%">Tên Nha khoa</th>--}}
{{--                                <th width="7%">Nhà cung cấp</th>--}}
{{--                                <th width="7%">Mã Qr</th>--}}
{{--                            </tr>--}}
{{--                        </thead>--}}
{{--                        <tbody>--}}
{{--                            @foreach ($services as $key => $service)--}}
{{--                                <tr>--}}
{{--                                    <td>{{ $key + 1 }}</td>--}}
{{--                                    <td>{{ $service->user->name }}</td>--}}
{{--                                    <td>{{ $service->client->name }}</td>--}}
{{--                                    <td>{{ date('d/m/Y', strtotime($service->client->birthday)) }}</td>--}}
{{--                                    <td>{{ $service->client->address }}</td>--}}
{{--                                    <td>{{ $service->client->phone }}</td>--}}
{{--                                    <td>{{ $service->product_name }}</td>--}}
{{--                                    <td>{{ $service->origin }}</td>--}}
{{--                                    <td>{{ date('d/m/Y', strtotime($service->date_buy)) }}</td>--}}
{{--                                    <td>{{ date('d/m/Y', strtotime($service->date_expired)) }}</td>--}}
{{--                                    <td>{{ $service->dental_name }}</td>--}}
{{--                                    <td>{{ $service->supplier }}</td>--}}
{{--                                    <td>--}}
{{--                                        <a target="_blank" href="{{ route('w.home.show_qrcode', $service->qrcode->url) }}">{{request()->getHttpHost()}}/bao-hanh/{{$service->qrcode->url }}</a>--}}
{{--                                    </td>--}}
{{--                                </tr>--}}
{{--                            @endforeach--}}
{{--                        </tbody>--}}
{{--                    </table>--}}
{{--                    {{ $services->links() }}--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}

@endsection
