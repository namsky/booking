@extends('admin.layouts.master')

@section('page_title', 'Cập nhật SEO cho trang ' . ucfirst($seo->page_slug))

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase">@yield('page_title')</span>
                    </div>
                    <div class="actions">
                        <a class="btn btn-circle btn-default" href="{{ route('ad.seo.index') }}" title="">Quay lại</a>
                        <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:void(0);" title="Toàn màn hình"></a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <form action="{{ route('ad.seo.update', $seo) }}" method="POST" @submit.prevent="onSubmit" enctype="multipart/form-data">
                        @method('PUT')
                        @csrf
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label for="name">Tên trang</label>
                                    <input type="text" id="name" class="form-control" name="name" v-validate="'required'" data-vv-as="&quot;Tên&quot;" value="{{ old('name', $seo->page_slug) }}" disabled>
                                </div>
                                <div class="form-group"  :class="[errors.has('description') ? 'has-error' : '']">
                                    <label for="description">Mô tả ngắn</label>
                                    <textarea id="description" class="form-control" name="description" style="resize: vertical;" rows="6" placeholder="Nhập mô tả ngắn...." v-validate="'required'" data-vv-as="&quot;Mô tả ngắn&quot;">{{ old('description') ? old('description') : $seo->description }}</textarea>
                                    <span class="help-block" v-if="errors.has('description')">@{{ errors.first('description') }}</span>
                                </div>
                                <div class="form-group"  :class="[errors.has('key_words') ? 'has-error' : '']">
                                    <label for="key_words">Từ khóa</label>
                                    <input type="text" id="key_words" class="form-control" name="key_words" value="{{ old('key_words') ? old('key_words') : $seo->key_words }}" placeholder="Nhập từ khóa...." v-validate="'required'" data-vv-as="&quot;Từ khóa&quot;">
                                    <span class="help-block" v-if="errors.has('key_words')">@{{ errors.first('key_words') }}</span>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="image">Hình ảnh <span class="required">*</span></label>
                                    <div>
                                        <div class="fileinput fileinput-{{ $seo->image ? 'exists' : 'new' }}" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail" style="width: 200px;">
                                                <img class="img-responsive" src="{{ asset('images/no_image.png') }}" alt="" />
                                            </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail" style="height: 200px">
                                                @if($seo->image)
                                                    <img class="img-responsive" src="{{ asset($seo->image) }}" alt="Preview banner" style="height:100%"/>
                                                @endif
                                            </div>
                                            <div>
                                                <span class="btn default btn-file">
                                                    <span class="fileinput-new">Chọn ảnh</span>
                                                    <span class="fileinput-exists">Đổi ảnh</span>
                                                    <input type="file" name="image">
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <button class="btn btn-primary">Cập nhật</button>
                            <a href="{{ route('ad.seo.index') }}" class="btn btn-default">Quay lại</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('css')
    <link rel="stylesheet" href="{{ asset('global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}"/>

@endpush

@prepend('scripts')
    <script src="{{ asset('global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
@endprepend
