@extends('admin.layouts.master')

@section('page_title', 'Cập nhật Admin')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase">@yield('page_title')</span>
                    </div>
                    <div class="actions">
                        <a class="btn btn-circle btn-default" href="{{ route('ad.user.index') }}" title="">Quay lại</a>
                        <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:void(0);" title="Toàn màn hình"></a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <form action="{{ route('ad.user.update', $user) }}" method="POST" @submit.prevent="onSubmit" enctype="multipart/form-data">
                        @method('PUT')
                        @csrf
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group" :class="[errors.has('name') ? 'has-error' : '']">
                                    <label for="name">Tên <span class="required">*</span></label>
                                    <input type="text" id="name" class="form-control" name="name" v-validate="'required'" data-vv-as="&quot;Tên&quot;" value="{{ old('name', $user->name) }}">
                                    <span class="help-block" v-if="errors.has('name')">@{{ errors.first('name') }}</span>
                                </div>
                                <div class="form-group">
                                    <label for="type">Hình thức đại lý</label>
                                    <select class="form-control" name="type" id="type">
                                        <option value="1" {{$user->type == 1 ? 'selected' : ''}}>Nha khoa</option>
                                        <option value="2" {{$user->type == 2 ? 'selected' : ''}}>Nhà cung cấp</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="code">Mã đại lý <span class="required">*</span></label>
                                    <input type="text" id="code" class="form-control" name="code" value="{{ old('code', $user->code) }}">
                                    <span class="help-block" v-if="errors.has('code')">@{{ errors.first('code') }}</span>
                                </div>
                                <div class="form-group form-check">
                                    <label class="form-check-label">
                                        <input name="display" class="form-check-input" {{$user->display == 1 ? 'checked' : ''}} type="checkbox"> Hiển thị ngoài trang chủ
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label for="email">Email <span class="required">*</span></label>
                                    <input type="text" id="email" class="form-control" name="email" value="{{ old('email', $user->email) }}" required>
                                    <span class="help-block" v-if="errors.has('email')">@{{ errors.first('email') }}</span>
                                </div>
                                <div class="form-group" :class="[errors.has('phone') ? 'has-error' : '']">
                                    <label for="phone">SĐT</label>
                                    <input type="text" id="phone" class="form-control" name="phone" data-vv-as="&quot;SĐT&quot;" value="{{ old('phone', $user->phone) }}">
                                    <span class="help-block" v-if="errors.has('phone')">@{{ errors.first('phone') }}</span>
                                </div>
                                <div class="form-group" :class="[errors.has('address') ? 'has-error' : '']">
                                    <label for="address">Đạ chỉ</label>
                                    <input type="text" id="address" class="form-control" name="address" data-vv-as="&quot;Địa chỉ&quot;" value="{{ old('address', $user->address) }}">
                                    <span class="help-block" v-if="errors.has('address')">@{{ errors.first('address') }}</span>
                                </div>
                                <div class="form-group" >
                                    <label for="description">Mô tả</label>
                                    <textarea  type="text" id="description" class="form-control tinymce" name="description">{!! old('description', $user->description) !!}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="password">Mật khẩu </label>
                                    <input type="text" id="password" class="form-control" name="password"  value="{{ old('password') }}">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="image">Hình ảnh <span class="required">*</span></label>
                                    <div>
                                        <div class="fileinput fileinput-{{ $user->avatar ? 'exists' : 'new' }}" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail" style="width: 200px;">
                                                <img class="img-responsive" src="{{ asset('images/no_image.png') }}" alt="" />
                                            </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail" style="height: 200px">
                                                @if($user->avatar)
                                                    <img class="img-responsive" src="{{ $user->avatar }}" alt="Preview banner"/>
                                                @endif
                                            </div>
                                            <div>
                                                <span class="btn default btn-file">
                                                    <span class="fileinput-new">Chọn ảnh</span>
                                                    <span class="fileinput-exists">Đổi ảnh</span>
                                                    <input type="file" name="image">
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <button class="btn btn-primary">Cập nhật</button>
                            <a href="{{ route('ad.user.index') }}" class="btn btn-default">Quay lại</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('css')
    <link rel="stylesheet" href="{{ asset('global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}"/>

@endpush

@prepend('scripts')
    <script src="{{ asset('global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
    @include('admin.lib.tinymce-setup')

@endprepend
