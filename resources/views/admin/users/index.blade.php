@extends('admin.layouts.master')

@section('page_title', 'Trang Danh sách các đại lý')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light portlet-datatable bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase">@yield('page_title')</span>
                    </div>
                    <div class="actions">
                        <a href="{{route('ad.user.create')}}" class="btn btn-circle btn-sm btn-primary"> <i class="fa fa-plus"></i> Tạo mới</a>
                        <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:void(0);" title=""></a>
                    </div>
                </div>
                <hr>
                <div class="portlet-title">
                    <form action="" method="GET">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Họ Tên</label>
                                    <input type="text" name="name" class="form-control" value="{{ old('name') }}" id="exampleInputEmail1" >
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Email</label>
                                    <input type="text" name="email" class="form-control" value="{{ old('email') }}" id="exampleInputEmail1" >
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="exampleInputEmail1" style="">&nbsp;</label>
                                    <button class="btn btn-primary form-control" type="submit">Tìm kiếm</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <hr>
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover" id="admin-table">
                        <thead>
                            <tr>
                                <th width="3%">ID</th>
                                <th width="10%">Tên</th>
                                <th width="7%">Mã đại lý</th>
                                <th width="7%">Hiển thị</th>
                                <th width="7%">Hình thức đại lý</th>
                                <th width="10%">Email</th>
                                <th width="7%">SĐT</th>
                                <th width="10%">Avatar</th>
                                <th width="10%">Ngày tạo</th>
                                <th width="5%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($users as $key => $user)
                            <tr>
                                <td >{{ $key+1 }}</td>
                                <td >{{ $user->name }}</td>
                                <td >{{ $user->code }}</td>
                                <td>{{ $user->display == 0 ? 'Không hiển thị' : 'Hiển thị trang chủ' }}</td>
                                <td >{{ ($user->type == 1)?'Nha khoa':'Nhà cung cấp' }}</td>
                                <td >{{ $user->email }}</td>
                                <td >{{ $user->phone }}</td>
                                <td >
                                    @if($user->avatar)
                                        <img style="max-height: 200px" class="img-responsive" src="{{ $user->avatar }}" alt="Preview banner"/>
                                    @else
                                        Chưa có ảnh đại diện
                                    @endif
                                </td>
                                <td >{{ \Carbon\Carbon::createFromDate($user->created_at)->format('d/m/Y') }}</td>
                                <td>
                                    <a class="btn btn-circle btn-sm btn-warning" href="{{ route('ad.user.edit', $user->id) }}"><i class="fa fa-pencil"></i></a>
                                    <form action="{{ route('ad.user.destroy', $user->id) }}" id="delete-course-item-form" style="display: inline-block" method="POST" onsubmit="return confirm('Bạn có chắc chắn muốn xóa?');">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-circle btn-sm btn-danger"><i class="fa fa-trash"></i></button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="text-center">
                        {{ $users->withQueryString()->links('web.layouts.paginate') }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <!-- END PAGE LEVEL PLUGINS -->
@endpush

@push('scripts')
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    {{-- <script src="{{ asset('global/plugins/select2/js/select2.min.js') }}" type="text/javascript"></script> --}}
    <!-- END PAGE LEVEL SCRIPTS -->
@endpush
