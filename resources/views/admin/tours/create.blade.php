@extends('admin.layouts.master')

@section('page_title', 'Tạo mới tour')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase">@yield('page_title')</span>
                    </div>
                    <div class="actions">
                        <a class="btn btn-circle btn-default" href="{{ route('ad.tour.index') }}" title="">Quay lại</a>
                        <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:void(0);" title="Toàn màn hình"></a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <form action="{{ route('ad.tour.store') }}" method="POST" @submit.prevent="onSubmit" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group" :class="[errors.has('name') ? 'has-error' : '']">
                                    <label for="name">Tên tour <span class="required">*</span></label>
                                    <input type="text" id="name" class="form-control" name="name" v-validate="'required'" data-vv-as="&quot;Tên tour&quot;" value="{{ old('name') }}" required>
                                    <span class="help-block" v-if="errors.has('name')">@{{ errors.first('name') }}</span>
                                </div>
                                <div class="form-group">
                                    <label for="type">Loại</label>
                                    <select class="form-control" name="type" id="type">
                                        <option value="1" selected>Ghế</option>
                                        <option value="2">Xe</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="from_id">Điểm đi</label>
                                    <select class="form-control" name="from_id" id="from_id">
                                        @foreach($from_locations as $from_location)
                                            <option {{old('from_id') == $from_location->id ? 'selected' : ''}} value="{{$from_location->id}}">{{$from_location->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group" :class="[errors.has('time_from') ? 'has-error' : '']">
                                    <label for="time_from">Giờ đi<span class="required">*</span></label>
                                    <input type="text" id="time_from" class="form-control" name="time_from" v-validate="'required'" data-vv-as="&quot;Giờ đi&quot;" value="{{ old('time_from') }}" required>
                                    <span class="help-block" v-if="errors.has('time_from')">@{{ errors.first('time_from') }}</span>
                                </div>
                                <div class="form-group">
                                    <label for="to_id">Điểm đến</label>
                                    <select class="form-control" name="to_id" id="to_id">
                                        @foreach($to_locations as $to_location)
                                            <option {{old('to_id') == $to_location->id ? 'selected' : ''}} value="{{$to_location->id}}">{{$to_location->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group" :class="[errors.has('time_to') ? 'has-error' : '']">
                                    <label for="time_to">Giờ đến<span class="required">*</span></label>
                                    <input type="text" id="time_to" class="form-control" name="time_to" v-validate="'required'" data-vv-as="&quot;Giờ đến&quot;" value="{{ old('time_to') }}" required>
                                    <span class="help-block" v-if="errors.has('time_to')">@{{ errors.first('time_to') }}</span>
                                </div>
                                <div class="form-group" :class="[errors.has('time') ? 'has-error' : '']">
                                    <label for="time">Thời gian di chuyển (giờ)<span class="required">*</span></label>
                                    <input type="text" id="time" class="form-control" name="time" v-validate="'required'" data-vv-as="&quot;Thời gian di chuyển&quot;" value="{{ old('time') }}" required>
                                    <span class="help-block" v-if="errors.has('time')">@{{ errors.first('time') }}</span>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-4 p-0" :class="[errors.has('price') ? 'has-error' : '']">
                                        <label for="price">Giá (USD/Per)<span class="required">*</span></label>
                                        <input type="text" id="price" class="form-control" name="price" v-validate="'required'" data-vv-as="&quot;Giá&quot;" value="{{ old('price',0) }}" required>
                                        <span class="help-block" v-if="errors.has('price')">@{{ errors.first('price') }}</span>
                                    </div>
                                    <div class="form-group col-md-4" :class="[errors.has('eur') ? 'has-error' : '']">
                                        <label for="eur">Giá (EUR/Per)<span class="required">*</span></label>
                                        <input type="text" id="eur" class="form-control" name="eur" v-validate="'required'" data-vv-as="&quot;Giá&quot;" value="{{ old('eur',0) }}" required>
                                        <span class="help-block" v-if="errors.has('eur')">@{{ errors.first('eur') }}</span>
                                    </div>
                                    <div class="form-group col-md-4 p-0" :class="[errors.has('gbp') ? 'has-error' : '']">
                                        <label for="gbp">Giá (GBP/Per)<span class="required">*</span></label>
                                        <input type="text" id="gbp" class="form-control" name="gbp" v-validate="'required'" data-vv-as="&quot;Giá&quot;" value="{{ old('gbp',0) }}" required>
                                        <span class="help-block" v-if="errors.has('gbp')">@{{ errors.first('gbp') }}</span>
                                    </div>
                                    <div class="form-group col-md-4 p-0" :class="[errors.has('aud') ? 'has-error' : '']">
                                        <label for="aud">Giá (AUD/Per)<span class="required">*</span></label>
                                        <input type="text" id="aud" class="form-control" name="aud" v-validate="'required'" data-vv-as="&quot;Giá&quot;" value="{{ old('aud',0) }}" required>
                                        <span class="help-block" v-if="errors.has('aud')">@{{ errors.first('aud') }}</span>
                                    </div>
                                    <div class="form-group col-md-4" :class="[errors.has('sgd') ? 'has-error' : '']">
                                        <label for="sgd">Giá (SGD/Per)<span class="required">*</span></label>
                                        <input type="text" id="sgd" class="form-control" name="sgd" v-validate="'required'" data-vv-as="&quot;Giá&quot;" value="{{ old('sgd',0) }}" required>
                                        <span class="help-block" v-if="errors.has('sgd')">@{{ errors.first('sgd') }}</span>
                                    </div>
                                    <div class="form-group col-md-4 p-0" :class="[errors.has('price') ? 'has-error' : '']">
                                        <label for="krw">Giá (KRW/Per)<span class="required">*</span></label>
                                        <input type="text" id="krw" class="form-control" name="krw" v-validate="'required'" data-vv-as="&quot;Giá&quot;" value="{{ old('krw',0) }}" required>
                                        <span class="help-block" v-if="errors.has('krw')">@{{ errors.first('krw') }}</span>
                                    </div>
                                </div>
                                <div class="form-group" :class="[errors.has('quantity_per') ? 'has-error' : '']">
                                    <label for="quantity_per">Số người tối đa<span class="required">*</span></label>
                                    <input type="text" id="quantity_per" class="form-control" name="quantity_per" v-validate="'required'" data-vv-as="&quot;Số người tối đa&quot;" value="{{ old('quantity_per',1) }}" required>
                                    <span class="help-block" v-if="errors.has('quantity_per')">@{{ errors.first('quantity_per') }}</span>
                                </div>
                                <div class="form-group" :class="[errors.has('travel_date') ? 'has-error' : '']">
                                    <label for="travel_date">Hạn đặt<span class="required">*</span></label>
                                    <input type="date" id="travel_date" class="form-control" name="travel_date" v-validate="'required'" data-vv-as="&quot;Hạn book&quot;" value="{{ old('travel_date') }}" required>
                                    <span class="help-block" v-if="errors.has('travel_date')">@{{ errors.first('travel_date') }}</span>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="image">Hình ảnh 1<span class="required">*</span></label>
                                    <div>
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail" style="width: 200px;">
                                                <img class="img-responsive" src="{{ asset('images/no_image.png') }}" alt="" />
                                            </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail" style="height: 200px"></div>
                                            <div>
                                                <span class="btn default btn-file">
                                                    <span class="fileinput-new">Chọn ảnh</span>
                                                    <span class="fileinput-exists">Đổi ảnh</span>
                                                    <input type="file" accept="image/*" name="image">
                                                </span>
                                                <a href="javascript:void(0);" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">Xóa</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="image">Hình ảnh 2<span class="required">*</span></label>
                                    <div>
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail" style="width: 200px;">
                                                <img class="img-responsive" src="{{ asset('images/no_image.png') }}" alt="" />
                                            </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail" style="height: 200px"></div>
                                            <div>
                                                <span class="btn default btn-file">
                                                    <span class="fileinput-new">Chọn ảnh</span>
                                                    <span class="fileinput-exists">Đổi ảnh</span>
                                                    <input type="file" accept="image/*" name="image_1">
                                                </span>
                                                <a href="javascript:void(0);" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">Xóa</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="description">Mô tả ngắn</label>
                                    <textarea id="description" class="form-control" name="description" style="resize: vertical;" rows="6" placeholder="Nhập mô tả ngắn....">{{ old('description') }}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="key_words">Từ khóa</label>
                                    <input type="text" id="key_words" class="form-control" name="key_words" value="{{ old('key_words') }}" placeholder="Nhập từ khóa....">
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <button class="btn btn-primary">Tạo mới</button>
                            <a href="{{ route('ad.tour.index') }}" class="btn btn-default">Quay lại</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('css')
{{--    <link href="https://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.css" rel="stylesheet"/>--}}
    <link href="{{ asset('global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css" />
@endpush

@prepend('scripts')
    <script src="{{ asset('global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
{{--    <script src="https://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.js"></script>--}}
{{--    <script>--}}
{{--        var timepicker = new TimePicker('time_from', {--}}
{{--            lang: 'en',--}}
{{--            theme: 'dark'--}}
{{--        });--}}
{{--        timepicker.on('change', function(evt) {--}}

{{--            var value = (evt.hour || '00') + ':' + (evt.minute || '00');--}}
{{--            evt.element.value = value;--}}

{{--        });--}}
{{--        var timepicker1 = new TimePicker('time_to', {--}}
{{--            lang: 'en',--}}
{{--            theme: 'dark'--}}
{{--        });--}}
{{--        timepicker1.on('change', function(evt_to) {--}}
{{--            console.log(evt_to)--}}

{{--            var value_to = (evt_to.hour || '00') + ':' + (evt_to.minute || '00');--}}
{{--            evt_to.element.value = value_to;--}}

{{--        });--}}
{{--    </script>--}}
    @include('admin.lib.tinymce-setup')

@endprepend

