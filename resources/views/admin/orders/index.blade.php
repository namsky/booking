@extends('admin.layouts.master')

@section('page_title', 'Trang Danh sách đơn hàng')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light portlet-datatable bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase">@yield('page_title')</span>
                    </div>
{{--                    <div class="actions">--}}
{{--                        <a href="{{route('ad.user.create')}}" class="btn btn-circle btn-sm btn-primary"> <i class="fa fa-plus"></i> Tạo mới</a>--}}
{{--                        <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:void(0);" title=""></a>--}}
{{--                    </div>--}}
                </div>
                <hr>
                <hr>
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover" id="admin-table">
                        <thead>
                        <tr>
                            <th width="3%">ID</th>
                            <th width="10%">Tên khách hàng</th>
                            <th width="7%">Số điện thoại</th>
                            <th width="7%">Tên tour</th>
                            <th width="7%">Số lượng người</th>
                            <th width="10%">Tổng tiền</th>
                            <th width="7%">Trạng thái</th>
                            <th width="10%">Ngày đặt</th>
                            <th width="5%">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($orders as $order)
                            <tr>
                                <td >{{ $order->id }}</td>
                                <td >{{ $order->name }}</td>
                                <td >{{ $order->phone }}</td>
                                <td>{{ $order->tour->name}}</td>
                                <td>{{ $order->quantity}}</td>
                                <td>{{ number_format($order->total_money)}} {{ $order->currency}}</td>
                                <td >
                                    @if($order->status == 0)
                                        Chưa xử lý
                                    @elseif($order->status == 1)
                                        Đang xử lý
                                    @else
                                        Đã xử lý
                                    @endif
                                </td>
                                <td >{{ \Carbon\Carbon::createFromDate($order->created_at)->format('H:i:s d/m/Y') }}</td>
                                <td>
                                    <a class="btn btn-circle btn-sm btn-warning" href="{{ route('ad.order.edit', $order->id) }}"><i class="fa fa-pencil"></i></a>
                                    {{-- <form action="{{ route('ad.order.destroy', $order->id) }}" id="delete-course-item-form" style="display: inline-block" method="POST" onsubmit="return confirm('Bạn có chắc chắn muốn xóa?');">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-circle btn-sm btn-danger"><i class="fa fa-trash"></i></button>
                                    </form> --}}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="text-center">
                        {{ $orders->withQueryString()->links('web.layouts.paginate') }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <!-- END PAGE LEVEL PLUGINS -->
@endpush

@push('scripts')
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    {{-- <script src="{{ asset('global/plugins/select2/js/select2.min.js') }}" type="text/javascript"></script> --}}
    <!-- END PAGE LEVEL SCRIPTS -->
@endpush
