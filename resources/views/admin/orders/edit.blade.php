@extends('admin.layouts.master')

@section('page_title', 'Cập nhật đơn hàng')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase">@yield('page_title')</span>
                    </div>
                    <div class="actions">
                        <a class="btn btn-circle btn-default" href="{{ route('ad.order.index') }}" title="">Quay lại</a>
                        <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:void(0);" title="Toàn màn hình"></a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="order_body">
                        <div class="order-left" style="width: 70%">
                            <h3 class="text-center font-weight-bold">Thông tin khách hàng</h3>
                            <hr>
                            <p>Tên khách hàng: {{$order->name}}</p>
                            <p>Số điện thoại: {{$order->phone}}</p>
                            <p>Email: {{$order->email}}</p>
                            <p>Địa chỉ đón: {{$order->address}}</p>
                            <p>Địa chỉ tới: {{$order->address_to}}</p>
                            <p>Ngày đón: {{$order->travel_date}}</p>
                            <p>Ghi chú: {{$order->note}}</p>
                        </div>
                        <div class="order-right" style="width: 30%">
                            <p>Tên tour : {{$order->tour->name}}</p>
                            <p>Thời gian di chuyển : {{$order->travel_hour}}</p>
                            <p>Số lượng người : {{$order->quantity}}</p>
                            <hr>
                            <p><strong>Thành tiền : {{number_format($order->total_money)}} {{$order->currency}}</strong></p>
                            <form action="{{ route('ad.order.update', $order) }}" method="POST">
                                @csrf
                                @method('PUT')
                                <div class="form-group">
                                    <label for="status">Trạng thái đơn hàng</label>
                                    <br>
                                    <select class="form-control" id="status" name="status">
                                        <option {{$order->status == 0 ? 'selected' : ''}} value="0">Chưa xử lý</option>
                                        <option {{$order->status == 1 ? 'selected' : ''}} value="1">Đang xử lý</option>
                                        <option {{$order->status == 2 ? 'selected' : ''}} value="2">Đã xử lý</option>
                                    </select>
                                </div>
                                <button class="btn btn-success" type="submit">Cập nhật</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('css')
    <style>
        .order_body {
            display: flex;
            justify-content: space-around;
        }
        .order-left {
            border: 1px solid #ccc;
            padding: 10px;
            margin-right: 10px;
        }
        .order-right {
            border: 1px solid #ccc;
            padding: 10px;
        }
    </style>
@endpush

@prepend('scripts')
@endprepend
