@extends('admin.layouts.master')

@section('page_title', 'Quản lý Bình luận')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light portlet-datatable bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase">@yield('page_title')</span>
                    </div>
                    <div class="actions">
                        <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:void(0);" title=""></a>
                    </div>
                </div>
                <hr>
                <div class="portlet-title">
                    <form action="" method="GET">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Từ khoá</label>
                                    <input type="text" name="q" class="form-control" value="{{ request('q') }}" id="exampleInputEmail1" >
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="">Ngày bắt đầu</label>
                                    <input type="date" name="startDate" class="form-control" value="{{ request('startDate') ? \Carbon\Carbon::create(request('startDate'))->format('Y-m-d') : ''  }}" id="" >
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="">Trạng thái</label>
                                    <select name="status" class="form-control" id="">
                                        <option value="">Tất cả</option>
                                        <option value="1" {{ request('status') == 1 ? 'selected' : '' }}>Đã phê duyệt</option>
                                        <option value="0" {{ request('status') == 0 ? 'selected' : '' }}>Chưa phê duyệt</option>
                                    </select>

                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="exampleInputEmail1" style="">&nbsp;</label>
                                    <button class="btn btn-primary form-control" type="submit">Tìm kiếm</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <hr>
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover" id="admin-table">
                        <thead>
                            <tr>
                                <th width="3%">ID</th>
                                <th width="10%">Tên người dùng</th>
                                <th width="10%">Tên bài bình luận</th>
                                <th width="7%">Nơi bình luận</th>
                                <th width="20%">Nội dung bình luận</th>
                                <th width="10%">Ngày</th>
                                <th width="5%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($comments as $comment)
                            <tr>
                                <td >{{ $comment->id }}</td>
                                <td >{{ $comment->user->name }}</td>
                                <td >{{ $comment->commentable->name }}</td>
                                <td >{{ $comment->commentable == 'App/Models/Blog' ? 'blog' : 'course' }}</td>
                                <td >{{ $comment->content }}</td>
                                <td>{{ \Carbon\Carbon::createFromDate($comment->created_at)->format('d/m/Y') }}</td>
                                <td>
                                    @if($comment->status == 0)
                                    <form action="{{ route('ad.comment.update', $comment) }}" style="display: inline-block" method="POST">
                                        @csrf
                                        @method('PATCH')
                                        <input type="hidden" name="status" value="1">
                                        <button class="btn btn-circle btn-sm btn-success"><i class="fa fa-check-circle"></i></button>
                                    </form>
                                    @else
                                        <form action="{{ route('ad.comment.update', $comment) }}" style="display: inline-block" method="POST">
                                            @csrf
                                            @method('PATCH')
                                            <input type="hidden" name="status" value="0">
                                            <button class="btn btn-circle btn-sm btn-danger"><i class="fa fa-times"></i></button>
                                        </form>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="text-center">
                        {{ $comments->withQueryString()->links('web.layouts.paginate') }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <!-- END PAGE LEVEL PLUGINS -->
@endpush

@push('scripts')
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    {{-- <script src="{{ asset('global/plugins/select2/js/select2.min.js') }}" type="text/javascript"></script> --}}
    <!-- END PAGE LEVEL SCRIPTS -->
@endpush
