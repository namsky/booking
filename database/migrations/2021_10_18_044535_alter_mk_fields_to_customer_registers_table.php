<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterMkFieldsToCustomerRegistersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_registers', function (Blueprint $table) {
            $table->string('address')->nullable();
            $table->string('ladipage')->nullable();
            $table->string('date_of_birth')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_registers', function (Blueprint $table) {
            $table->dropIfExists('address');
            $table->dropIfExists('ladipage');
            $table->dropIfExists('date_of_birth');
        });
    }
}
