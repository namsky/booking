<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddEmailAddressToTravelDateTravelHourQtyPerToOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->string('email');
            $table->text('address_to');
            $table->date('travel_date');
            $table->string('travel_hour');
            $table->string('qty_per');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('email');
            $table->dropColumn('address_to');
            $table->dropColumn('travel_date');
            $table->dropColumn('travel_hour');
            $table->dropColumn('qty_per');
        });
    }
}
