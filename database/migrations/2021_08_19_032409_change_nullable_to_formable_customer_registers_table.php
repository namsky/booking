<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeNullableToFormableCustomerRegistersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_registers', function (Blueprint $table) {
            $table->string('formable_type')->nullable()->change();
            $table->unsignedInteger('formable_id')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_registers', function (Blueprint $table) {
            $table->string('formable_type')->nullable(false)->change();
            $table->unsignedInteger('formable_id')->nullable(false)->change();
        });
    }
}
