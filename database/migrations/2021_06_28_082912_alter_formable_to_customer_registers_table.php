<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterFormableToCustomerRegistersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_registers', function (Blueprint $table) {
            $table->string('formable_type');
            $table->unsignedInteger('formable_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_registers', function (Blueprint $table) {
            $table->dropColumn('formable_type');
            $table->dropColumn('formable_id');
        });
    }
}
