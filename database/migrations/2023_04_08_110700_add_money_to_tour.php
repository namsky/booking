<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMoneyToTour extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tours', function (Blueprint $table) {
            $table->integer('eur')->default(0)->nullable()->after('price');
            $table->integer('gbp')->default(0)->nullable()->after('price');
            $table->integer('aud')->default(0)->nullable()->after('price');
            $table->integer('sgd')->default(0)->nullable()->after('price');
            $table->integer('krw')->default(0)->nullable()->after('price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tours', function (Blueprint $table) {
            $table->dropColumn('eur');
            $table->dropColumn('gbp');
            $table->dropColumn('aud');
            $table->dropColumn('sgd');
            $table->dropColumn('krw');
        });
    }
}
