<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class SeoPagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['page_slug' => 'book-a-bus','description' => 'Nhập mô tả ngắn', 'key_words' => 'Nhập từ khóa','image' => '/images/no_image.png'],
            ['page_slug' => 'about-us','description' => 'Nhập mô tả ngắn', 'key_words' => 'Nhập từ khóa','image' => '/images/no_image.png'],
            ['page_slug' => 'terms-and-translation','description' => 'Nhập mô tả ngắn', 'key_words' => 'Nhập từ khóa','image' => '/images/no_image.png'],
            ['page_slug' => 'mui-ne-jeep-tour','description' => 'Nhập mô tả ngắn', 'key_words' => 'Nhập từ khóa','image' => '/images/no_image.png'],
            ['page_slug' => 'proof-of-onward-travel-for-your-vietnam-visa','description' => 'Nhập mô tả ngắn', 'key_words' => 'Nhập từ khóa','image' => '/images/no_image.png'],
            ['page_slug' => 'contact','description' => 'Nhập mô tả ngắn', 'key_words' => 'Nhập từ khóa','image' => '/images/no_image.png'],
        ];
        \App\Models\SeoPage::insert($data);

    }
}
