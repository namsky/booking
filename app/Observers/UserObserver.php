<?php

namespace App\Observers;

use App\Models\User;
use App\Models\DataUser;
use Str;
use App\Components\Partners\Partner;
use App\Components\Partners\Wm;
use App\Components\Partners\Ae;
use App\Components\Partners\Cq9;
use App\Components\Partners\Jdb;
use App\Components\Partners\Allbet;

class UserObserver
{

    protected $wm;
    protected $ae;
    protected $cq9;
    protected $jdb;
    protected $allbet;

    public function __construct(Wm $wm, Ae $ae, Cq9 $cq9, Jdb $jdb, Allbet $allbet)
    {
        $this->wm = $wm;
        $this->ae = $ae;
        $this->cq9 = $cq9;
        $this->jdb = $jdb;
        $this->allbet = $allbet;
    }


    /**
     * Handle the User "created" event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
//    public function created(User $user)
//    {
//        $data['user_id'] = $user->id;
//        $data['user']   = 'ibet66com' . Str::lower(Str::random(6));
//        $data['username'] = $data['user'];
//        $data['user'] = $data['user'];
//        $data['password'] = Str::random(10);
//
//        $wm = [
//            'cmd' => config('partners.wm.api.register.cmd'),
//            'vendorId'  => config('partners.wm.wm_vendorid'),
//            'signature' => config('partners.wm.wm_signature'),
//            'user'      => $data['user'],
//            'password'  =>  $data['password'],
//            'username'  =>  $data['user'],
//            'rakeback'  => true,
//            'timestamp' => time(),
//            'syslang'   => 1
//        ];
//
//        $ae = ['form_params' =>
//            [
//                'cert' => config('partners.ae.cert'),
//                "agentId" => config('partners.ae.agentId'),
//                "language" => config('partners.ae.language'),
//                "currency" => config('partners.ae.currency'),
//                "betLimit" => config('partners.ae.api.register.betLimit'),
//                "userId" => $data['user'],
//
//                "userName" => $data['user']
//            ]
//        ];
//
//        $cq9 =  [
//            "account" => $data['user'],
//            "nickname"=> $data['user'],
//            "password"=> $data['password']
//        ];
//
//        $jdb =  [
//            "action" => config('partners.jdb.api.register.action'),
//            'ts' => (int)now()->format('Uv'),
//            'parent' => config('partners.jdb.account'),
//            "uid"=> $data['user'],
//            "name"=> $data['user']
//        ];
//
//        $allbet =  [
//            "agent" => config('partners.allbet.agentId'),
//            "player" => $data['user']
//        ];
//
//        // $this->allbet->register($allbet);
//
//        $this->jdb->register($jdb);
//
//        $this->wm->register($wm);
//
//        $this->ae->register($ae);
//
//        $this->cq9->register($cq9);
//
//        DataUser::create($data);
//    }

    /**
     * Handle the User "updated" event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function updated(User $user)
    {
        //
    }

    /**
     * Handle the User "deleted" event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function deleted(User $user)
    {
        //
    }

    /**
     * Handle the User "restored" event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function restored(User $user)
    {
        //
    }

    /**
     * Handle the User "force deleted" event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function forceDeleted(User $user)
    {
        //
    }
}
