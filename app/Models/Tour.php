<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tour extends Model
{
    use HasFactory;
    protected $fillable = ['name', 'type', 'from_id','to_id','time','service','thumb','image_1','price','quantity_per', 'slug', 'travel_date','time_from', 'time_to','eur','gbp','aud','sgd','krw','description', 'key_words'];

    public function location($id) {
        $location = Location::find($id);
        return $location->name;
    }

    public function fromLocation(){
        return $this->belongsTo(Location::class,'from_id','id');
    }

    public function toLocation(){
        return $this->belongsTo(Location::class,'to_id','id');
    }

    public function orders(){
        return $this->hasMany(Order::class,'tour_id','id');
    }
}
