<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;
    protected $fillable = [
        'tour_id', 'name', 'address', 'phone', 'quantity', 'total_money', 'status', 'note','email','address_to','travel_date','travel_hour','qty_per','currency'
    ];

    public function product() {
        return $this->belongsTo(Product::class);
    }
    public function tour() {
        return $this->belongsTo(Tour::class);
    }
}
