<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    use HasFactory;
    protected $fillable = ['name','slug','type', 'image', 'description', 'key_words'];

    public function tours(){
        return $this->hasMany(Tour::class,'from_id','id');
    }
}
