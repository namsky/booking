<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SeoPage extends Model
{
    use HasFactory;
    protected $fillable = [
        'page_slug','description', 'key_words', 'image',
    ];

//    public function getSeoByName($name = ''){
////        dd($this);
//        $seo = $this->where('page_slug',$name)->first();
//        return $seo;
//    }
}
