<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CustomerLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {

        if(! customer()->check()){

            return redirect()->route('us.login.index');
        }

        return $next($request);
    }
}
