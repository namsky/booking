<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCourseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'string|required|min:8|max:255',
            'description' => 'string|required',
            'content' => 'string|required',
            'total_time' => 'string|required',
            'total_lessons' => 'string|required',
            'auth' => 'required',
            'link' => 'required',
        ];
    }
}
