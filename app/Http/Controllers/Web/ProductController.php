<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;

class ProductController extends Controller
{
    public function index() {
        $products = Product::orderBy('id','DESC')->where('type',1)->paginate(10);
        return view('web.products.list_product', compact('products'));
    }
    public function single($slug) {
        $product = Product::where('slug',$slug)->first();
        return view('web.products.single')->with(compact('product'));
    }
}
