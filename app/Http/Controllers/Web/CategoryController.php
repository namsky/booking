<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Location;
use App\Models\Tour;
use Illuminate\Http\Request;
use Carbon\Carbon;

class CategoryController extends Controller
{
    public function index(Location $location)
    {
        $date = Carbon::now()->toDateString();
        $tours = Tour::where('from_id',$location->id)->where('travel_date','>=', $date)->orderBy('id','DESC');
        $count_tour = $tours->get()->count();
        $seats_taxis = clone $tours;
        $private_taxis = $tours->where('type',2)->get();
        $seats_taxis = $seats_taxis->where('type',1)->get();
        $date = Carbon::now()->addDays(3)->toDateString();
        return view('web.category')->with(compact('location','seats_taxis', 'private_taxis','tours','date'));
    }
    public function category_child($from_id, $to_id) {
        $date = Carbon::now()->toDateString();
        $tour = new Tour();
        $name_from = $tour->location($from_id);
        $name_to = $tour->location($to_id);
        $tours = Tour::where(['from_id'=>$from_id,'to_id'=>$to_id])->where('travel_date','>=', $date)->orderBy('id','DESC');
        $count_tour = $tours->get()->count();
        $seats_taxis = clone $tours;
        $private_taxis = $tours->where('type',2)->get();
        $seats_taxis = $seats_taxis->where('type',1)->get();
        $date = Carbon::now()->addDays(3)->toDateString();
        $location = null;
        $image = Location::find($from_id)->image;
        return view('web.category')->with(compact('image','location','tours','seats_taxis', 'private_taxis','date','name_from','name_to'));
    }
}
