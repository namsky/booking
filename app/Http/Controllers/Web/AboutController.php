<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\SeoPage;
use Illuminate\Http\Request;

class AboutController extends Controller
{
    public function index()
    {
        $seo = SeoPage::where('page_slug','about-us')->first();
        return view('web.about',compact('seo'));
    }
}
