<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Models\Order;

class OrderController extends Controller
{
    public function index(Request $request) {
        $product = Product::find($request->product_id);
        $amount = $request->amount;
        if($product) {
            return view('web.orders.index')->with(compact('product','amount'));
        } else {
            return redirect()->back()->with('error', 'Sản phẩm không tồn tại');
        }
    }

    public function store(Request $request) {
        if($request->name && $request->address && $request->phone && $request->product_id && $request->amount) {
            $product = Product::find($request->product_id);
            if($product) {

                $data = [
                  'product_id' => $request->product_id,
                  'name' => $request->name,
                  'address' => $request->address,
                  'phone' => $request->phone,
                  'quantity' => $request->amount,
                  'status' => 0,
                  'note' => $request->note,
                  'total_money' => $product->price * $request->amount,
                ];
                Order::create($data);
                return redirect()->route('w.home')->with('success', 'Đặt hàng thành công!');
            } else {
                return redirect()->back()->with('error', 'Không tìm thấy sản phẩm!');
            }
        } else {
            return redirect()->back()->with('error', 'Vui lòng điền đủ thông tin!');
        }
    }
}
