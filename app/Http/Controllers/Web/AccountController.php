<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Models\Bank;
use App\Models\CustomerBank;
use Illuminate\Http\Request;
use App\Http\Requests\ResetPassRequest;
use App\Models\User;
use App\Models\Payment;
use App\Http\Requests\PaymentRequest;

class AccountController extends Controller
{
    public function index()
    {
        $bank = CustomerBank::where('user_id','=',customer()->user()->id)->first();

        return $this->view('account.view')->with(compact('bank'));
    }

    public function resetpass(ResetPassRequest $request) {

        $user = customer()->user();
        $hasher = app('hash');

        if ($hasher->check($request->oldpassword, $user->password)) {

           if($request->newpassword == $request->confirmpassword) {

               $user->password = bcrypt($request->newpassword);
               $user->save();

               session()->flash('success', 'Đổi mật khẩu thành công!');

           } else {
            session()->flash('error', 'Mật khẩu mới không khớp nhau!');
           }

        } else {
            session()->flash('error', 'Mật khẩu cũ không đúng!');
        }

        return redirect()->route('w.account');
    }

    public function recharge() {
//        $recharges = Payment::where('type', '=', 0)->where('user_id', '=', customer()->user()->id)->orderBy('id', 'DESC')->take(10)->get();
        return $this->view('account.recharge');
    }
    public function withdraw() {
//        $withdraws = Payment::where('type', '=', 1)->where('user_id', '=', customer()->user()->id)->orderBy('id', 'DESC')->take(10)->get();
        return $this->view('account.withdraw');
    }
    public function transaction() {
        $recharges = Payment::where('type', '=', 0)->where('user_id', '=', customer()->user()->id)->orderBy('id', 'DESC')->take(10)->get();

        $withdraws = Payment::where('type', '=', 1)->where('user_id', '=', customer()->user()->id)->orderBy('id', 'DESC')->take(10)->get();
        return $this->view('account.transaction')->with(compact('recharges','withdraws'));
    }
}
