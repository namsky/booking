<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Blog;
use App\Models\Category;
use App\Models\Partner;
use App\Models\SeoPage;
use App\Models\User;
use Illuminate\Http\Request;
use App\Models\Config;
use Illuminate\Database\Eloquent\Builder;
use App\Models\Location;
use App\Models\Tour;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;

class HomeController extends Controller
{

    public function index()
    {
        //Điểm đi
        $from_locations = Location::orderBy('name','DESC')->where('type',1)->get();
        //Điểm đến
        $to_locations = Location::orderBy('name','DESC')->where('type',2)->get();
        $seo = SeoPage::where('page_slug','book-a-bus')->first();
        return view('web.home')->with(compact('from_locations','to_locations','seo'));
    }

    public function view_price(Request $request) {
        if(!$request->from_location || !$request->to_location || !$request->quantity_per || !$request->travel_date) {
            return redirect()->back();
        }

        $tours = Tour::where('from_id',$request->from_location)->where('to_id',$request->to_location)->where('quantity_per','>=',$request->quantity_per)->where('travel_date','>=',$request->travel_date);
        $count_tour = $tours->get()->count();
        $seats_taxis = clone $tours;

        $private_taxis = $tours->where('type',2)->get();

        $seats_taxis = $seats_taxis->where('type',1)->get();
        //Điểm đi
        $from_location = Location::find($request->from_location);
        //Điểm đến
        $to_location = Location::find($request->to_location);

        // Tên điểm đi
        $name_form = Location::find($request->from_location);
        $name_to = Location::find($request->to_location);

        // Số người đi
        $quantity_per = $request->quantity_per;
        return view('web.view_price')->with(compact('count_tour', 'seats_taxis', 'private_taxis', 'from_location','to_location','name_form', 'name_to', 'quantity_per'));
    }

    public function change_price(Request $request) {
        $request->session()->put('currency', $request->select_money);
        return redirect()->back();
    }

    public function find_to_location(Request $request) {
        $tours = Tour::where('from_id',$request->from)->get();
        $data = [];
        $list_to = [];
        $slug = '';
        if ($tours) {
            foreach ($tours as $tour) {
                $location = Location::find($tour->to_id);
                if($slug != $location->slug) {
                    $list_to[] = $location;
                    $slug = $location->slug;
                }
            }
            $data = [
              'error' => 'success',
                'data' => $list_to
            ];
        } else {
            $data = [
                'error' => 'error'
            ];
        }
        return response($data, 200)
            ->header('Content-Type', 'text/plain');
    }

    public function check_limit_per(Request $request) {
        $tour = Tour::find($request->tour_id);
        if($tour->quantity_per < $request->qty) {
            $data = [
                'error' => 'error',
                'qty' => $tour->quantity_per
            ];
        } else {
            $data = [
                'error' => 'success'
            ];
        }
        return response($data, 200)
            ->header('Content-Type', 'text/plain');
    }

}
