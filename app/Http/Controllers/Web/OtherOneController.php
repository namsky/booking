<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\SeoPage;
use Illuminate\Http\Request;

class OtherOneController extends Controller
{
    public function index()
    {
        $seo = SeoPage::where('page_slug','terms-and-translation')->first();
        return view('web.other_one',compact('seo'));
    }
}
