<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Blog;
use App\Models\Category;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function view($slug)
    {
        $blog = Blog::where('slug', '=', $slug)->limit(1)->first();
        return view('web.blog.blog_single')->with(compact('blog'));
    }
    public function list_blog()
    {
        $category = Category::where('slug','dieu-kien-bao-hanh')->first();
        if($category) {
            $blogs = Blog::orderBy('id','DESC')->where('category_id','!=',$category->id)->paginate(9);
        } else $blogs = Blog::orderBy('id','DESC')->paginate(9);

        return view('web.blog.list_blog', compact('blogs'));
    }
}
