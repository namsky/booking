<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Tour;
use Illuminate\Http\Request;
use App\Models\Order;

class BookingController extends Controller
{
    public function index(Tour $tour, $quantity_per, $travel_date)
    {
        return view('web.booking')->with(compact('tour','quantity_per','travel_date'));
    }

    public function post_booking(Request $request) {
//        check if travel date is less than the current date
        $today = date('Y-m-d');
        if($request->travel_date === $today){
            return back()->with('warning','This is a last minute booking and cannot be made through the website now - but you may be able to book this by calling us directly: (+84) 0943 696 611 - Phone/Zalo');
        }

        if($request->travel_date < $today){
            return back()->with('warning','The booking date/time is in the past - please update and try again!');
        }

//        create order
        $tour = Tour::find($request->tour_id);

        if(session('currency') == 'eur') {
            $price = $tour->eur;
            $current = 'EUR';
        }elseif(session('currency') == 'gbp') {
            $price = $tour->gbp;
            $current = 'GBP';
        }elseif(session('currency') == 'aud') {
            $price = $tour->aud;
            $current = 'AUD';
        } elseif(session('currency') == 'sgd') {
            $price = $tour->sgd;
            $current = 'SGD';
        }elseif(session('currency') == 'krw') {
            $price = $tour->krw;
            $current = 'KRW';
        } else {
            $price = $tour->price;
            $current = 'USD';
        }
        $data['tour_id'] = $request->tour_id;
        $data['name'] = $request->name;
        $data['email'] = $request->email;
        $data['address'] = $request->address;
        $data['address_to'] = $request->address_to;
        $data['travel_date'] = $request->travel_date;
        $data['travel_hour'] = $request->travel_hour;
        $data['qty_per'] = $request->quantity_per;
        $data['phone'] = $request->phone;
        $data['quantity'] = $request->quantity_per;
        $data['status'] = 0;

        if($tour->type == 2){
            $data['total_money'] = $price;
        }else{
            $data['total_money'] = $request->quantity_per*$price;
        }

        $data['currency'] = $current;
        $order = Order::create($data);
        return redirect()->route('w.home')->with('success','Success');
    }
}
