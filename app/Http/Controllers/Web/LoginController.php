<?php

namespace App\Http\Controllers\Web;

use Auth;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use Illuminate\Validation\Rules\Unique;

class LoginController extends Controller
{

    public function getLogin()
    {
        return $this->view('login.login');
    }

    public function getRegister()
    {
        return $this->view('login.register');
    }

    public function register(RegisterRequest $request)
    {
        $user = $request->only('name', 'email', 'phone');
        $user['password'] = \Hash::make($request->password);

        if(! User::create($user)) {
            return redirect()->back()->with('error', 'Đăng ký thất bại');
        }

        $login = [
            'name' => $request->name,
            'password' => $request->password
        ];

        if (customer()->attempt($login)) {
            return redirect()->route('w.home')->with('success', 'Đăng ký thành công');
        } else {
            return redirect()->back()->with('error', 'Đăng nhập thất bại');
        }

    }

    public function login(Request $request)
    {
        $login = [
            'name' => $request->name,
            'password' => $request->password
        ];

        if (customer()->attempt($login)) {
            return redirect()->route('w.home')->with('success', 'Đăng nhập thành công');
        } else {
            return redirect()->back()->with('error', 'Đăng nhập thất bại');
        }

    }

}
