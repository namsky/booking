<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use App\Models\Location;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Jenssegers\Agent\Agent;
use App\Models\Partner;
use App\Models\Product;
use App\Models\Brand;
use Illuminate\Support\Facades\View;


class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        //Điểm đi
        $from_home_locations = Location::orderBy('name','DESC')->where('type',1)->get();
        //Điểm đến
        $to_home_locations = Location::orderBy('name','DESC')->where('type',2)->get();
        //Lấy danh sách tuyến xe 
        $merge_datas = array();
        if($from_home_locations) {
            foreach ($from_home_locations as $key=>$from_home_location) {
                if(!empty($to_home_locations)) {
                    foreach ($to_home_locations as $key1=>$to_home_location) {
                        if($from_home_location->slug != $to_home_location->slug) {
                            $merge_datas[$key.$key1]['from'] = $from_home_location;
                            $merge_datas[$key.$key1]['to'] = $to_home_location;
                        }
                    }
                }
            }
        }
        $blog_news = Blog::orderBy('id','DESC')->orderBy('id','DESC')->take(5)->get();
        // Lấy mệnh giá tiền
        // dd(session('currency'));

        if (session()->has('currency')) {
            $currency = session('currency');
        } else $currency = 'price';
        // dd(session('currency'));
        session()->put('blog_news', $blog_news);
        session()->put('from_home_locations', $from_home_locations);
        session()->put('to_home_locations', $to_home_locations);
        session()->put('merge_datas', $merge_datas);
        session()->put('currency', $currency);
        View::share('blog_news', $blog_news);
        View::share('from_home_locations', $from_home_locations);
        View::share('to_home_locations', $to_home_locations);
        View::share('merge_datas', $merge_datas);
        View::share('currency', $currency);
    }

    public function uploadFile($file)
    {
        $filenameWithExt = $file->getClientOriginalName();
        //Get just filename
        $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
        // Get just ext
        $extension = $file->getClientOriginalExtension();
        // Filename to store
        $fileNameToStore = $filename.'_'.time().'.'.$extension;
        // Upload Image
        $path = $file->storeAs('/public/photos/blog/images', $fileNameToStore);
        return str_replace('public', '/storage', $path);
    }
}
