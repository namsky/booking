<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateServiceRequest;
use App\Http\Requests\UpdateServiceRequest;
use App\Models\Client;
use App\Models\Product;
use App\Models\Qrcode;
use App\Models\Service;
use App\Models\User;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        // if ($request->product_name) {
        //     $services = Service::orWhere('product_name', 'like', '%' . $request->product_name . '%')->paginate(10)->withQueryString();
        // }

        // if ($request->phone) {
        //     $clients = Client::orWhere('phone', 'like', '%' . $request->phone . '%')->paginate(10)->withQueryString();
        // }

        // dd($service);


        // return view('customer.home', compact('services', 'clients'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id, Qrcode $qrcodes)
    {
        $qrcodes = Qrcode::where('user_id', '=', Customer()->user()->id)->where('status', '==', 0)->get();
        $products = Product::where('type','=',1)->where('display',1)->get();
        // dd($qrcodes);
        return view('customer.create', compact('id', 'qrcodes','products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateServiceRequest $request)
    {
        $this->validate($request, [
            'qrcode_id' => 'required',
        ]);
        $this->validate($request, [
            'product_id' => 'required',
        ]);

        $data = $request->all();
        if($request->image != null){
            $data['image'] = $this->uploadFile($request->image);
        }else{
            $data['image'] = asset('/images/defaul-user-image.jpeg');
        }

        $data['user_id'] = Customer()->user()->id;
        $result = Service::create($data);

        if (isset($result->id)) {
            $qrcode = Qrcode::where('id', '=', $result->qrcode_id)->first();
            $qrcode->status = 1;
            $qrcode->save();
            return redirect()
            ->route('us.home.index')
            ->with('success', 'Thêm dich vu thành công');
        }else {
            return redirect()
            ->route('us.home.index')
            ->with('error', 'Có lỗi vui lòng thử lại sau');
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Service $service)
    {
        $clients = Client::all();
        $qrcodes = Qrcode::where('user_id', '=', Customer()->user()->id)->where('status', '==', 0)->get();
        $products = Product::where('type','=',1)->get();
        return view('customer.edit', compact('service', 'clients', 'qrcodes', 'products'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateServiceRequest $request, Service $service)
    {
        $data = $request->all();
        $this->validate($request, [
            'product_id' => 'required',
        ]);
        if ($request->image) {
            $request->validate([
                'image' => 'required|mimes:jpeg,png,jpg,gif,svg|max:5120',
            ]);

            $path = $this->uploadFile($request->image);
            $data['image'] = $path;
        }

        $service->update($data);
        return redirect()
            ->route('us.home.index')
            ->with('success', 'Cập nhật thành công');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Service $service)
    {

        // if ($service->id) {
        //     $qrcode = Qrcode::where('id', '=', $service->qrcode_id)->first();
        //     $qrcode->status = 0;
        //     $qrcode->save();
        //     $service->delete();
        //     return redirect()
        //     ->route('us.home.index')
        //     ->with('success', 'Xóa thành công');
        // }else {
        //     return redirect()
        //     ->route('us.home.index')
        //     ->with('error', 'Có lỗi vui lòng thử lại sau');
        // }

//        $service->qrcode()->delete();
        $service->delete();
            return redirect()
            ->route('us.home.index')
            ->with('success', 'Xóa thành công');

    }
}
