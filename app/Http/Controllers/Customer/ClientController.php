<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateClientRequest;
use App\Models\Client;
use App\Models\Qrcode;
use App\Models\Service;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $clients = Client::orwhere('phone', 'LIKE', "%$request->phone%")->where('name', 'LIKE', "%$request->name%")->where('user_id','=',Customer()->user()->id)->paginate(20)->withQueryString();
        return view('customer.client.index', compact('clients'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $service
        return view('customer.client.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateClientRequest $request)
    {
        $data = $request->all();
        $data['user_id'] = customer()->user()->id;
        Client::create($data);
        return redirect()->route('us.client.index')->with('success', 'Thêm khách hàng thành công');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client)
    {
        return view('customer.client.detail', compact('client'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client)
    {
        return view('customer.client.edit', compact('client'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Client $client)
    {
        $data = $request->all();
        $client->update($data);
        return redirect()->route('us.client.index')->with('success', 'Cập nhật thành công');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Client $client)
    {
        $service = Service::where('client_id','=',$client->id)->first();
        if(isset($service->id)) {
            return redirect()->route('us.client.index')->with('error', 'Khách hàng đã sử dụng dịch vụ. Không thể xóa !');
        } else {
            $client->delete();
            return redirect()->route('us.client.index')->with('success', 'Xóa thành công');
        }
    }
}
