<?php

namespace App\Http\Controllers\Customer;

use App\Models\Client;
use App\Models\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Qrcode;
use App\Models\User;

class HomeController extends Controller
{
    public function index(Request $request)
    {   
        if($request->phone) {
            $client = Client::where('phone','=',$request->phone)->first();
            if ($client) {
                $services = Service::where('user_id','=',Customer()->user()->id)->where('client_id','=',$client->id)->paginate(20);
            }
            $services = Service::where('user_id','=',Customer()->user()->id)->paginate(20);

        } else $services = Service::where('user_id','=',Customer()->user()->id)->paginate(20);

        $qrcodes = Qrcode::all();
        return view('customer.home', compact('services', 'qrcodes'));
    }

    
}
