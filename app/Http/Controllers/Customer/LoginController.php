<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;

class LoginController extends Controller
{
    public function index() {
        return view('customer.login');
    }
    public function login(Request $request) {
        $login = $request->only('email', 'password');
        if(customer()->attempt($login)) {
            session()->flash('success', 'Đăng nhập thành công');
            return redirect()->route('us.home.index')->with('success', 'Đăng nhập thành công');
        } else{
            session()->flash('error', 'Email hoặc Mật khẩu không chính xác');
            return redirect()->route('us.login');
        }
    }
    public function logout() {
        customer()->logout();
        return redirect()->route('us.login');
    }
}
