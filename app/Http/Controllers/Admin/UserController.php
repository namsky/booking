<?php

namespace App\Http\Controllers\Admin;

use App\Models\Client;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $users = User::orderBy('id', 'DESC')->paginate(20);
        return view('admin.users.index', compact('users'));
    }

    public function create()
    {
        return view('admin.users.create');
    }

    public function store(CreateUserRequest $request)
    {
        $data = $request->only('name', 'email', 'phone', 'gender', 'code', 'type', 'description', 'address');
        $data['password'] = Hash::make($request->password);
        if($request->display != null && $request->display == "on") {
            $data['display'] = 1;
        }
        if($request->image != null){
            $data['avatar'] = $this->uploadFile($request->image);
        }else{
            $data['avatar'] = asset('/images/defaul-user-image.jpeg');
        }

        User::create($data);
        return redirect()->route('ad.user.index')->with('success', 'Thêm tài khoản đại lý thành công');

    }


    public function edit(User $user)
    {
        return view('admin.users.edit', compact('user'));
    }

    public function update(Request $request, User $user)
    {
        $data = $request->only('name', 'email', 'phone', 'gender', 'code', 'type', 'description', 'address');
        if ($request->password) {
            $request->validate([
                'password' => 'required|min:6|max:15'
            ]);
            $data['password'] = Hash::make($request->password);
        }
        if($request->display != null && $request->display == "on") {
            $data['display'] = 1;
        } else $data['display'] = 0;
//        dd($request->image);
        if ($request->image != null) {
            $request->validate([
                'image' => 'required|mimes:jpeg,png,jpg,gif,svg|max:5120'
            ]);
            $path = $this->uploadFile($request->image);
            $data['avatar'] = $path;
        }

        $user->update($data);
        session()->flash('success', 'Thành công');

        return redirect()->route('ad.user.index');
    }

    public function destroy(User $user)
    {
        $client = Client::where('user_id','=',$user->id)->first();
        if(isset($client->id)) {
            session()->flash('error', 'Đại lý đã có khách hàng. Không thể xóa !');
        } else {
            $user->delete();
            session()->flash('success', 'Thành công');
        }
        return redirect()->route('ad.user.index');
    }

    public function showresetpass() {
        return view('customer.client.resetpass');
    }
    public function resetpass(Request $request) {
        $this->validate($request, [
            'password' => 'required|min:6|max:15',
        ]);
        $data['password'] = Hash::make($request->password);
        $return = Customer()->user()->update($data);
        session()->flash('success', 'Thành công');
        return redirect()->route('us.home.index');
    }
}
