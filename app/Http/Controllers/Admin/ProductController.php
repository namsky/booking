<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Http\Requests\Admin\CreateProductRequest;
use Illuminate\Support\Str;

class ProductController extends Controller
{
    public function index(Request $request)
    {
        $products = Product::orderBy('id', 'DESC')->paginate(10);

        return view('admin.products.index', compact('products'));
    }

    public function create()
    {
        return view('admin.products.create');
    }

    public function store(CreateProductRequest $request)
    {
        $data = $request->only('name', 'content', 'description', 'type', 'price');
        if($request->display != null && $request->display == "on") {
            $data['display'] = 1;
        }
        if($request->image != null){
            $data['image'] = $this->uploadFile($request->image);
        }else{
            $data['image'] = asset('/images/defaul-user-image.jpeg');
        }

        $data['slug'] = Str::slug($request->name);

        Product::create($data);
        session()->flash('success', 'Thành Công');

        return redirect()->route('ad.product.index');
    }

    public function edit(Product $product)
    {
        return view('admin.products.edit', compact('product'));
    }

    public function update(Request $request, Product $product)
    {
        $data = $request->only('name', 'description', 'content', 'type', 'price');
        if($request->display != null && $request->display == "on") {
            $data['display'] = 1;
        } else $data['display'] = 0;
        if ($request->image != null) {
            $request->validate([
                'image' => 'required|mimes:jpeg,png,jpg,gif,svg|max:5120'
            ]);
            $path = $this->uploadFile($request->image);
            $data['image'] = $path;
        }

        $data['slug'] = Str::slug($request->name);

        $product->update($data);

        session()->flash('success', 'Thành công');

        return redirect()->route('ad.product.index');
    }

    public function destroy(Product $product)
    {
        $product->delete();
        session()->flash('success', 'Thành Công!');
        return redirect()->route('ad.product.index');
    }
}
