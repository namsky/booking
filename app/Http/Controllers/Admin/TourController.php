<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Location;
use Illuminate\Http\Request;
use App\Models\Tour;
use Illuminate\Support\Str;

class TourController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tours = Tour::orderBy('created_at','DESC')->paginate(20);

        return view('admin.tours.index', compact('tours'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Điểm đi
        $from_locations = Location::orderBy('name','DESC')->where('type',1)->get();
        //Điểm đến
        $to_locations = Location::orderBy('name','DESC')->where('type',2)->get();
        return view('admin.tours.create', compact('from_locations', 'to_locations'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->only('name','type','from_id', 'to_id', 'time', 'price', 'quantity_per', 'travel_date','time_from','time_to','eur','gbp','aud','sgd','krw','description', 'key_words');
        if($request->image != null){
            $data['thumb'] = $this->uploadFile($request->image);
        }else{
            $data['thumb'] = asset('/images/defaul-user-image.jpeg');
        }
        if($request->image_1 != null){
            $data['image_1'] = $this->uploadFile($request->image_1);
        }else{
            $data['image_1'] = asset('/images/defaul-user-image.jpeg');
        }

        $data['slug'] = Str::slug($request->name);
        $data['service'] = $request->name;

        Tour::create($data);
        session()->flash('success', 'Thành Công');

        return redirect()->route('ad.tour.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Tour $tour)
    {
        //Điểm đi
        $from_locations = Location::orderBy('name','DESC')->where('type',1)->get();
        //Điểm đến
        $to_locations = Location::orderBy('name','DESC')->where('type',2)->get();
        return view('admin.tours.edit', compact('tour','from_locations', 'to_locations'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tour $tour)
    {
        $data = $request->only('name','type', 'from_id', 'to_id', 'time', 'price', 'quantity_per', 'travel_date','time_from','time_to','eur','gbp','aud','sgd','krw','description', 'key_words');
        if($request->image != null){
            $data['thumb'] = $this->uploadFile($request->image);
        }
        if($request->image_1 != null){
            $data['image_1'] = $this->uploadFile($request->image_1);
        }

        $data['slug'] = Str::slug($request->name);
        $data['service'] = $request->name;

        $tour->update($data);
        session()->flash('success', 'Thành Công');

        return redirect()->route('ad.tour.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tour $tour)
    {
        if($tour->orders->count()){
            session()->flash('error','Tour ' . $tour->name .' đã được book. Không được xóa');
            return back();
        }

        $tour->delete();
        session()->flash('success', 'Thành Công');
        return redirect()->route('ad.tour.index');
    }
}
