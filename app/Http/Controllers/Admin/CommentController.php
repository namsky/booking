<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Comment;
use Illuminate\Http\Request;

class CommentController extends Controller
{

    public function index(Request $request)
    {
        $comments = Comment::orderBy('id', 'desc');

        if(isset($request->q)) {
            $comments = $comments->where('content', 'like', "%$request->q%");
        }
        if(isset($request->startDate)) {
            $comments = $comments->whereDate('created_at', '>=', $request->startDate);
        }
        if(isset($request->status)) {
            $comments = $comments->where('status', $request->status);
        }

        $comments = $comments->paginate(30);


        return view('admin.comment.index', compact('comments'));

    }

    public function update(Request $request, Comment $comment)
    {
        $comment = $comment->update([
            'status' => $request->status
        ]);

        if(! $comment){
            return redirect()->back()->with('error', 'Lỗi hệ thống');
        }

        return redirect()->back()->with('success', 'Thành công');

    }
}
