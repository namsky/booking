<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Models\Location;
use Illuminate\Support\Str;

class LocationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $locations = Location::orderBy('created_at','DESC')->paginate(20);
        return view('admin.locations.index', compact('locations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.locations.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $request->validate([
                'name'=>'max:255',
                'key_words'=>'max:255',
                'description'=>'max:255',
            ],[
                'name.max'=>'Tên điểm không vượt quá 255 ký tự',
                'description.max'=>'Mô tả ngắn không vượt quá 255 ký tự',
                'key_words.max'=>'Từ khóa không vượt quá 255 ký tự',
            ]);
            $data = $request->only('name', 'type', 'description', 'key_words');
            $data['slug'] = Str::slug($request->name);
            if($request->image != null){
                $data['image'] = $this->uploadFile($request->image);
            }else{
                $data['image'] = asset('/images/defaul-user-image.jpeg');
            }
            Location::create($data);

            session()->flash('success', 'Thành công');
            return redirect()->route('ad.locations.index');
        } catch(Exception $e) {
            session()->flash('error', 'Thất bại');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Location $location)
    {
        return view('admin.locations.edit', compact('location'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Location $location)
    {
        try {
            $request->validate([
                'name'=>'max:255',
                'description'=>'max:255',
                'key_words'=>'max:255',
            ],[
                'name.max'=>'Tên điểm không vượt quá 255 ký tự',
                'description.max'=>'Mô tả ngắn không vượt quá 255 ký tự',
                'key_words.max'=>'Từ khóa không vượt quá 255 ký tự',
            ]);

            $data = $request->only('name','type','description', 'key_words');
            $data['slug'] = Str::slug($request->name);

            if($request->image != null){
                $data['image'] = $this->uploadFile($request->image);
            }
            $location->update($data);

            session()->flash('success', 'Thành công');
            return redirect()->route('ad.locations.index');
        } catch(Exception $e) {
            session()->flash('error', 'Thất bại');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Location $location)
    {
        $location->delete();
        session()->flash('success', 'Thành Công');
        return redirect()->route('ad.locations.index');
    }
}
