<?php

namespace App\Http\Controllers\Admin;
use App\Models\SeoPage;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
class SeoPagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages = SeoPage::paginate(7);
        return view('admin.seo_pages.index',compact('pages'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(SeoPage $seo)
    {
        return view('admin.seo_pages.edit',compact('seo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SeoPage $seo)
    {
        try {
            $request->validate([
                'key_words'=>'required|max:255',
                'description'=>'required|max:255',
            ],[
                'description.max'=>'Mô tả ngắn không vượt quá 255 ký tự',
                'key_words.max'=>'Từ khóa không vượt quá 255 ký tự',
            ]);

            $data = $request->only( 'description', 'key_words');

            if($request->image != null){
                $data['image'] = $this->uploadFile($request->image);
            }else{
                $data['image'] = asset('/images/no_image.png');
            }

            $seo->update($data);
            session()->flash('success', 'Thành công');
            return redirect()->route('ad.seo.index');
        } catch(Exception $e) {
            session()->flash('error', 'Thất bại');
            return redirect()->back();
        }
    }

}
