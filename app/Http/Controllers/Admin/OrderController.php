<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function index() {
        $orders = Order::orderBy('id','DESC')->paginate(10);
        return view('admin.orders.index')->with(compact('orders'));
    }

    public function edit(Order $order) {
        return view('admin.orders.edit')->with(compact('order'));
    }

    public function update(Request $request, Order $order) {

        $data = $request->only('status');
        $order->update($data);
        session()->flash('success', 'Thành công');

        return redirect()->route('ad.order.index');
    }
}
