<?php

namespace App\Components\Partners;

use Illuminate\Support\Str;

class Wm extends Partner
{
    protected function getBaseUri()
    {
        return $this->getConfigData('domain') . '/';

    }

    public function register($data)
    {
        $uri = $this->genUri($data);

        return $this->makeRequest(config('partners.wm.endpoint') . $uri, config('partners.wm.api.register.method'));
    }

    public function loginGame($data)
    {
        $uri = $this->genUri($data);

        return $this->makeRequest(config('partners.wm.endpoint') . $uri, config('partners.wm.api.login.method'));

    }

    public function genUri($data)
    {
        $uri = "?";
        $index = 0;

        foreach($data as $key => $item) {

            if($index == 0) {

                $index++;
                $uri .= $key . '='.$item;
            } else {
                $uri .= '&' . $key . '='.$item;
            }
        };

        return $uri;
    }
}
