<?php

namespace App\Components\Partners;

use Illuminate\Support\Str;

class Jili extends Partner
{
    protected function getBaseUri()
    {
        return $this->getConfigData('domain') . '/';

    }

    public function register($data)
    {
        $data['keyG'] = $this->genKey();
        // dd($data, config('partners.jili.endpoint') . config('partners.jili.api.register.url'));
        return $this->makeRequest(config('partners.liji.endpoint') . config('partners.jili.api.register.uri') , config('partners.jili.api.register.method'), $data);
    }

    // public function login($data)
    // {
    //     return $this->makeRequest(config('partners.ae.endpoint') . config('partners.ae.api.login.uri') , config('partners.ae.api.login.method'), $data);
    // }

    public function genKey()
    {
        return Str::random(6) . md5(date('ymd') . config('partners.liji.agentId') . config('partners.liji.agentKey')) . Str::random(6);
    }
}

