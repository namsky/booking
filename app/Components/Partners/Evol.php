<?php

namespace App\Components\Partners;

use Illuminate\Support\Str;

class Evol extends Partner
{
    protected function getBaseUri()
    {
        return $this->getConfigData('domain') . '/';

    }

    public function register($data)
    {
        $uri = config('partners.evol.endpoint') . '/ua/v1/' .  config('partners.evol.key') . '/' . config('partners.evol.token');
        return $this->makeRequest($uri , config('partners.evol.api.register.method'), 
        [\GuzzleHttp\RequestOptions::JSON => $data]
    );
    }

}

