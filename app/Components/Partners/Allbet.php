<?php

namespace App\Components\Partners;

use Illuminate\Support\Str;

class Allbet extends Partner
{
    protected function getBaseUri()
    {
        return $this->getConfigData('domain') . '/';

    }

    public function register($data)
    {
        $apiURL = config('partners.allbet.endpoint');
        $path = config('partners.allbet.api.register.url');
        $method = config('partners.allbet.api.register.method');

        $data['player'] = $data['player'] . config('partners.allbet.suffix');

        $options = $this->createRequest($data, $path, $method);
        
        $url = $apiURL . $path;

        $context  = stream_context_create($options);
        $result = file_get_contents($url, false, $context);

        return json_decode($result);
    }

    public function login($data)
    {
        $apiURL = config('partners.allbet.endpoint');
        $path = config('partners.allbet.api.login.url');
        $method = config('partners.allbet.api.login.method');

        $data['player'] = $data['player'] . config('partners.allbet.suffix');

        $options = $this->createRequest($data, $path, $method);
        
        $url = $apiURL . $path;
        // dd($options);

        $context  = stream_context_create($options);
        $result = file_get_contents($url, false, $context);

        return json_decode($result);
    }

   
    public function createRequest($data, $path, $method)
    {
        //Please replace with correct url. The differen solution you choose will have different apiURL
        $apiURL = config('partners.allbet.endpoint') ;
        $path = config('partners.allbet.api.register.url');
        // $httpMethod = $method;
        $contentType = "application/json";

        //Please replace with your Operator ID
        $propertyId = config('partners.allbet.operatorId');
        //Please replace with your AllBet API Key
        $allbetApiKey = config('partners.allbet.agentKey');
        $date   = new \DateTime();
        $requestTime = $date->format('d M Y H:m:s T'); // "Wed, 28 Apr 2021 06:13:54 UTC"; 

        //Build the request parameters according to the API documentation
        // $requestBodyString =  json_encode([
        //         "agent" => config('partners.allbet.agentId'),
        //         "player" => "ibet66est005" . config('partners.allbet.suffix')
        //     ]);
        $requestBodyString =  json_encode($data);
        $contentMD5 =  base64_encode(pack('H*', md5($requestBodyString)));
        //The steps to generate HTTP authorization headers
        $stringToSign = $method . "\n"
        . $contentMD5 . "\n"
        . $contentType . "\n"
        . $requestTime . "\n"
        . $path;
        // dd($stringToSign);
        //Use HMAC-SHA1 to sign and generate the authorization
        $deKey = base64_decode($allbetApiKey);
        $hash_hmac = hash_hmac("sha1", $stringToSign, $deKey, true);
        $encrypted = base64_encode($hash_hmac);
        $authorization = "AB " . $propertyId . ":" . $encrypted;
        
        //Send the Http request
        
        $options = array(
            'http' => array(
            'method'  => $method,
            'content' => $requestBodyString,
            'header' =>  "Content-Type: application/json\r\n" .
                "Accept: application/json\r\n" .
                "Authorization:" . $authorization . "\r\n" .
                "Date:" . $requestTime . "\r\n" .
                "Content-MD5:" . $contentMD5 . "\r\n"
            )
        );

        return $options;

        // $context  = stream_context_create($options);
        // $result = file_get_contents($url, false, $context);
        // $response = json_decode($result);
        // dd($response);
    }


    public function tt()
    {
        $options = [
            "content" => '{"agent":"j54cna","player":"ibet66est0052rk"}',
            "header" =>
            [   "Content-Type" => "application/json",
                "Accept"=> "application/json",
                "Authorization" =>"AB 9098577:wVM7vnCQ/FdRy97/CORTUa9uKB4=",
                "Date"=>"23 Aug 2022 22:08:33 +07",
                "Content-MD5" =>"2sc+QL2jw3sviuFr/VP8yw=="
            ]
        ];
        $data = $this->makeRequest("https://mw2.apidemo.net:8443/CheckOrCreate", "POST", $options);
        dd($data);
    }
}

