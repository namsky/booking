<?php

namespace App\Components\Partners;

use Illuminate\Support\Str;

class Ae extends Partner
{
    protected function getBaseUri()
    {
        return $this->getConfigData('domain') . '/';

    }

    public function register($data)
    {
        return $this->makeRequest(config('partners.ae.endpoint') . config('partners.ae.api.register.uri') , config('partners.ae.api.register.method'), $data);
    }

    public function login($data)
    {
        return $this->makeRequest(config('partners.ae.endpoint') . config('partners.ae.api.login.uri') , config('partners.ae.api.login.method'), $data);
    }
}

