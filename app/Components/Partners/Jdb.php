<?php

namespace App\Components\Partners;

use Illuminate\Support\Str;

class Jdb extends Partner
{
    protected function getBaseUri()
    {
        return $this->getConfigData('domain') . '/';

    }

    public function register($jdb)
    {
        $jdb = json_encode($jdb);

        $jdb = $this->padString($jdb);

        $encrypted = openssl_encrypt($jdb, 'AES-128-CBC', config('partners.jdb.key'), OPENSSL_NO_PADDING, config('partners.jdb.iv'));
        $encrypted = base64_encode($encrypted);
        $encrypted = str_replace(array('+','/','=') , array('-','_','') , $encrypted);

        $uri = config('partners.jdb.endpoint');
        // dd($uri . $dataHash);
        return $this->makeRequest($uri . $encrypted, config('partners.jdb.api.register.method'));
    }

    public function loginGame($jdb)
    {
        $jdb = json_encode($jdb);

        $jdb = $this->padString($jdb);

        $encrypted = openssl_encrypt($jdb, 'AES-128-CBC', config('partners.jdb.key'), OPENSSL_NO_PADDING, config('partners.jdb.iv'));
        $encrypted = base64_encode($encrypted);
        $encrypted = str_replace(array('+','/','=') , array('-','_','') , $encrypted);

        $uri = config('partners.jdb.endpoint');
        // dd($uri . $dataHash);
        return $this->makeRequest($uri . $encrypted, config('partners.jdb.api.register.method'));
    }

    public function padString($source)
    {
        $paddingChar = ' ';
        $size = 16;
        $x = strlen($source) % $size;
        $padLength = $size - $x;
        for ($i = 0;$i < $padLength;$i++)
        {
            $source .= $paddingChar;
        }
        return $source;
    }
}

