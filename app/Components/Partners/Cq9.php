<?php

namespace App\Components\Partners;

use Illuminate\Support\Str;

class Cq9 extends Partner
{
    protected function getBaseUri()
    {
        return $this->getConfigData('domain') . '/';

    }

    public function register($data)
    {
        $uri = config('partners.cq9.endpoint') . config('partners.cq9.api.register.url');

        $options['headers'] = $this->getHeaderSend();
        
        $data = array_merge($options, ['form_params' => $data]);
        
        return $this->makeRequest($uri , config('partners.cq9.api.register.method'), $data);
    }

    public function login($data)
    {
        $uri = config('partners.cq9.endpoint') . config('partners.cq9.api.login.url');

        $options['headers'] = $this->getHeaderSend();
        
        $data = array_merge($options, ['form_params' => $data]);
        
        return $this->makeRequest($uri , config('partners.cq9.api.login.method'), $data);
    }

    public function getLinkGame($data)
    {
        $uri = config('partners.cq9.endpoint') . config('partners.cq9.api.getLinkGame.url');

        $options['headers'] = $this->getHeaderSend();
        
        $data = array_merge($options, ['form_params' => $data]);
        
        return $this->makeRequest($uri , config('partners.cq9.api.getLinkGame.method'), $data);
    }
    

    private function getHeaderSend()
    {
        return [
            'Authorization' => config('partners.cq9.key'),
            'Content-Type'  => config('partners.cq9.api.register.contentType')
        ];
    }

}

