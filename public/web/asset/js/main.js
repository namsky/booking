$(function () {
    var Accordion = function (el, multiple) {
        this.el = el || {};
        this.multiple = multiple || false;

        // Variables privadas
        var links = this.el.find(".link");
        // Evento
        links.on(
            "click",
            { el: this.el, multiple: this.multiple },
            this.dropdown
        );
    };

    Accordion.prototype.dropdown = function (e) {
        var $el = e.data.el;
        ($this = $(this)), ($next = $this.next());

        $next.slideToggle();
        $this.parent().toggleClass("open");

        if (!e.data.multiple) {
            $el.find(".submenu")
                .not($next)
                .slideUp()
                .parent()
                .removeClass("open");
        }
    };

    var accordion = new Accordion($("#accordion"), false);
});

// capcha
// var onloadCallback = function() {
//   grecaptcha.render('g-recaptcha', {
//       '6Ldp6CglAAAAAOtN5YU_jauyLprUOQxsRt2IliI3' : '6Lev1BsgAAAAAOCEtZQzQOMr123456'
//   });
// };

// var recaptchaCallback = function () {
//   let submit = document.getElementById('submit-btn')
//   submit.classList.remove('disabled');
// }

// form = document.getElementById('dmca-report-form');
// form.addEventListener('submit', function (e){
//     if (grecaptcha && grecaptcha.getResponse().length !== 0) {
//         this.submit();
//     } else {
//         alert('Please check captcha');
//     }
//     e.preventDefault();
// })

$(document).ready(function () {
    // Scroll btn
    $("#scrollBtn").click(function (event) {
        $form = $("#sec_9").offset();
        $("html,body").animate({ scrollTop: $form.top }, 1800);
    });

    //select 2
    $(document).ready(function () {
        $(".js-example-basic-single").select2({
            allowClear: true,
        });
    });

    // show-image
    $(".image-show").click(function () {
        $(".image-modal-show img").attr("src", `${this.src}`);
    });


    // set thong bao
    // setTimeout(function(){
    //     $('.alert').hide();
    // }, 2000);


    // form dang ky
    $("#contactform").submit(function (e) {
        // value form
        e.preventDefault();
        var name = $("#name").val();
        var phone = $("#phone").val();
        var email = $("#email").val();
        var message = $("#message").val();
        if (name == "" || phone == "" || email == "" || message == "") {
            Swal.fire({
                title: "Vui lòng nhập đủ thông tin !",
                icon: "error",
                confirmButtonText: "Đóng",
            });
        } else {
            // message telegram
            var message = `Khách hàng liên hệ:     Tên: ${name}        - Số điện thoại: ${phone}       - Email: ${email}      - Nội dung: ${message} `;
            $.ajax({
                type: "GET",
                url:
                    "https://api.telegram.org/bot6242531942:AAFzNYBaCpwQXW1kVE9bCHIRTBUG-22DI6k/sendMessage?chat_id=-993589079&text=" +
                    message,
                data: "", // serializes the form's elements.
                success: function (data) {
                    if (data.ok == true) {
                        Swal.fire(
                            "Gửi Thành Công!",
                            "Cảm ơn bạn đã gửi liên hệ!",
                            "success"
                        );
                    } else {
                        Swal.fire({
                            title: "Gửi thất bại!",
                            text: "Vui lòng thử lại sau!",
                            icon: "error",
                            confirmButtonText: "Cool",
                        });
                    }
                },
            });
        }
    });
});


// set min date for input date
let today = new Date();
let minDate = today.toISOString().split('T')[0];
$(".min-date").attr("min", minDate);

// set default date  for the travel date
let threeDateNext = today.setDate(today.getDate() + 3);
threeDateNext = today.toISOString().split('T')[0];

$("#travel-to").attr("value", threeDateNext);
